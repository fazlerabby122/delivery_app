import 'package:flutter/material.dart';
import 'package:delivery_app/app/app_local.dart';
import 'package:delivery_app/ui/sharedWidgets/app_dialog_widget.dart';
import 'package:delivery_app/ui/views/settings/settings_viewmodel.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_themes/stacked_themes.dart';


class SettingsView extends StatelessWidget {
  SettingsView({Key? key}) : super(key: key);
  final GlobalKey<AppDialogState> _key = GlobalKey();


  Widget themeSettingsContent(BuildContext context,SettingsViewModel model){
    var theme = Theme.of(context);
    return Center(
      child: Wrap(
        spacing: 30,
        runSpacing: 20,
        alignment: WrapAlignment.start,
        direction: Axis.horizontal,
        children: model.themes
            .map(
              (themeData) => GestureDetector(
            // onTap: () => model.setTheme(themeData),
            onTap: () {
              getThemeManager(context).selectThemeAtIndex(themeData.index);
            },
            child: Container(
              width: 100.w,
              padding: EdgeInsets.symmetric(
                  horizontal: 5.h, vertical: 10.h),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.r),
                  color: theme.colorScheme.secondary),
              child: Text(themeData.title,style: theme.textTheme.bodyLarge,),
            ),
          ),
        )
            .toList(),
      ),
    );
  }

  Widget languageSettingsContent(BuildContext context,SettingsViewModel model,void Function(void Function()) setBuildState){
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            child: const Text('English'),
            onPressed: () {
              setBuildState (() {
                model.localization.translate('en');
              });
            },
          ),
        ),
        const SizedBox(width: 8.0),
        Expanded(
          child: ElevatedButton(
            child: const Text('বাংলা'),
            onPressed: () {
              setBuildState (() {
                model.localization.translate('bn');
              });
            },
          ),
        ),
        const SizedBox(width: 8.0),
        Expanded(
          child: ElevatedButton(
            child: const Text('اَلْعَرَبِيَّةُ'),
            onPressed: () {
              setBuildState (() {
                model.localization.translate('ar', save: false);
              });
            },
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<SettingsViewModel>.reactive(
      viewModelBuilder: () => SettingsViewModel(),
      builder: (context, model, child) => Scaffold(
        backgroundColor: theme.colorScheme.background,
        appBar: AppBar(title: Text('Settings')),
        body: ListView(
          children: [
            ListTile(
              title: Text('Theme Settings'),
              trailing: Icon(Icons.edit),
              onTap: () async{
                await showDialog<Map<String, dynamic>>(
                    barrierDismissible: true,
                    context: context,
                    builder: (BuildContext dialogContext) =>
                        StatefulBuilder(
                            builder: (builderContext, setBuildState) {
                              return AppDialog<SettingsViewModel>(
                                key:_key,
                                viewModel: model,
                                title: 'Theme settings',
                                titleStyle: theme.textTheme.titleSmall,
                                description: 'choose app theme',
                                customWidget: Column(
                                  children: [
                                    Container(
                                        height: 100.h,
                                        child: themeSettingsContent(context,model)
                                    ),
                                    SizedBox(height: 10.h,),
                                  ],
                                ),
                                cancelButtonTitle: 'cancel',
                                cancelButtonTitleColor: Colors.red,
                                confirmButtonTitle: 'confirm',
                                confirmButtonTitleColor: Colors.black,
                                confirmButtonOnPress: (){
                                  Navigator.of(builderContext).pop();
                                },
                                cancelButtonOnPress: (){
                                  Navigator.of(builderContext).pop();
                                },
                              );})
                );

              },
            ),
            Divider(height: 2.h,),
            ListTile(
              title: Text('Language Settings'),
              trailing: Icon(Icons.edit),
              onTap: () async{
                await showDialog<Map<String, dynamic>>(
                    barrierDismissible: true,
                    context: context,
                    builder: (BuildContext dialogContext) =>
                        StatefulBuilder(
                            builder: (builderContext, setBuildState) {
                              return AppDialog<SettingsViewModel>(
                                key:_key,
                                viewModel: model,
                                title: AppLocale.chooseLanguage.getString(context),
                                titleStyle: theme.textTheme.titleSmall,
                                description: 'choose app language',
                                customWidget: Column(
                                  children: [
                                    Container(
                                        height: 100.h,
                                        child: languageSettingsContent(builderContext,model,setBuildState)
                                    ),
                                    SizedBox(height: 10.h,),
                                  ],
                                ),
                                cancelButtonTitle: AppLocale.cancel.getString(context),
                                cancelButtonTitleColor: Colors.red,
                                confirmButtonTitle: AppLocale.confirm.getString(context),
                                confirmButtonTitleColor: Colors.black,
                                confirmButtonOnPress: (){
                                  Navigator.of(builderContext).pop();
                                },
                                cancelButtonOnPress: (){
                                  Navigator.of(builderContext).pop();
                                },
                              );})
                );

              },
            ),
            Divider(height: 2.h,),
          ],
        )
      ),
    );
  }
}
