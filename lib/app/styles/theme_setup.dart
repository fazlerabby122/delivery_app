import 'package:flutter/material.dart';
import 'package:delivery_app/app/styles/app_theme.dart';
import 'package:delivery_app/app/styles/primary_theme.dart';
import 'package:delivery_app/app/styles/themes/base_theme.dart';

List<ThemeData> getThemes() {
  return [
    ThemeManager.craeteTheme(AppThemeLight()),
    ThemeManager.craeteTheme(AppThemeDark()),
    // AppTheme.themePrimary(),
    // AppTheme.themeSecondary(),
    // AppTheme.themeDark()
    // ThemeData(backgroundColor: Colors.blue, accentColor: Colors.yellow),
  ];
}
