

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';


class AppDatePicker extends StatelessWidget{

  AppDatePicker({
    required this.showTitleActions,
    this.isEditable =false,
    this.value,
    this.valueStyle,
    this.hintText = '',
    this.hintStyle,
    this.minDate,
    this.maxDate,
    this.headerColor,
    required this.backgroundColor,
    required this.itemStyle,
    required this.doneStyle,
    required this.cancelStyle,
    required this.dateFormat,
    this.controller,
    required this.onChanged,
    required this.onConfirm,
    Key? key,
  }) : super(key: key);

  final bool showTitleActions;
  bool? isEditable;
  final String? value;
  final TextStyle? valueStyle;
  final String hintText;
  final TextStyle? hintStyle;
  final DateTime? minDate;
  final DateTime? maxDate;
  final Color? headerColor;
  final Color backgroundColor;
  final TextStyle itemStyle;
  final TextStyle doneStyle;
  final TextStyle cancelStyle;
  final DateFormat dateFormat;
  final Function(DateTime) onChanged;
  final Function(DateTime)  onConfirm;
  final TextEditingController? controller;


  @override
  Widget build(BuildContext context) {
    return
      SizedBox(
          width:250,
          child:Row(
            children: [
              SizedBox(width: 150.w,
                child: TextField(
                  textAlign: TextAlign.center,
                  enabled: isEditable,
                  controller:TextEditingController(text: value != 'null' ? dateFormat.format(DateTime.parse(value!)):'',),
                  style: valueStyle,
                  decoration: InputDecoration(
                    hintText: hintText,
                    hintStyle:hintStyle,
                  ),
                ),),
              SizedBox(width: 10.w),
              IconButton(
                icon: Icon(Icons.date_range,size: 30.w,),
                onPressed: () {
                  DatePicker.showDatePicker(context,
                    showTitleActions: showTitleActions,
                    minTime: minDate,
                    maxTime: maxDate,
                    theme: DatePickerTheme(
                      headerColor: headerColor,
                      backgroundColor: backgroundColor,
                      itemStyle: itemStyle,
                      doneStyle: doneStyle,
                      cancelStyle: cancelStyle,
                    ),
                    onChanged: (date) => onChanged(date),
                    onConfirm: (date) => onConfirm(date),
                    currentTime: DateTime.now(),
                    locale: LocaleType.en);
                },
              )
            ],
          )
      );
  }

}