class User {
  String? id;
  String? fullName;
  String? email;
  String? userToken;
  DateTime? expiryDate;
  String? avatar;

  User({this.id, this.fullName, this.email, this.userToken,required DateTime this.expiryDate,this.avatar});

  User.fromData(Map<String, dynamic> data)
      : id = data['id'],
        fullName = data['fullName'],
        email = data['email'],
        userToken = data['userToken'],
        expiryDate = data['expires_in'],
        avatar = data['avatar'];

  Map<String, dynamic> toJson() {
    return {
      'user_id': id,
      'fullName': fullName,
      'email': email,
      'user_token': userToken,
      'expiryDate':expiryDate,
      'avatar':avatar,
    };
  }
}