


import 'package:delivery_app/domain/models/cartItem.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:delivery_app/domain/models/product_item.dart';
import 'package:delivery_app/ui/partialViews/cart/cartViewModel.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:stacked/stacked.dart';

class CartButton extends StatefulWidget {
  // Product product;
CartItem product;
  CartButton({required this.product});

  @override
  _CartButtonState createState() => _CartButtonState();
}

class _CartButtonState extends State<CartButton>{


  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<CartViewModel>.reactive(
        viewModelBuilder: () => CartViewModel(),
        onViewModelReady: (model) async{
          await model.fetchAndSetCartItems();
          model.getNewCartItem();
          // if(model.cartItems != null){
          //   newCartItem = Map.fromIterable(model.cartItems!, key: (v) => v.productId, value: (v) => v.quantity);
          // }
        },
        builder: (context,model,child) =>
            model.isBusy ? Center(child: SizedBox(
              // child: CircularProgressIndicator(strokeWidth: 1.w),
              height: 15.h,
              width: 15.w,
            )):

            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                model.cartItems != null &&
                model.newCartItem != null && model.newCartItem.length > 0 && model.newCartItem.keys.contains(widget.product.id) ?
                Expanded(child:Center(child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.add_circle_outline,size: 20.0.r,),
                      color: theme.colorScheme.secondary,
                      onPressed:
                      // () async{
                      // !_baseViewModel.branchSettingsItem.allowNegativeStockSale &&
                      //     double.parse(viewModel.cartItems.firstWhere((d) => d.productId == product.id).quantity.toString()) >=
                      //         double.parse(viewModel.product.quantityLeft) ? null:
                          () async{
                        // await model.addItem(
                        //     widget.product.id,
                        //     widget.product.title,
                        //     widget.product.unit,
                        //     double.parse(widget.product.quantityLeft),
                        //     widget.product.price,
                        //     widget.product.avgUnitCost,
                        //     widget.product.isNonInventory,
                        //     widget.product.salesAccountsGroupId,
                        //     // product.discount,
                        //     0.0,
                        //     widget.product.discountId,
                        //     widget.product.discountType,
                        //     widget.product.perUnitDiscount,
                        //     widget.product.vatRate
                        // );
                        model.getNewCartItem();

                        await model.getTotalAmount();
                        print('item count from plus button ' + model.cartItems!.length.toString());
                        print('total amount from plus button ' + model.totalAmount.toString());

                          },
                    ),
                    model.cartItems!.length == 1 && model.cartItems!.first.productId == '1' ? SizedBox(width:0.0,height:0.0):
                    // newCartItem.firstWhere((d) => d.productId == widget.product.id).quantity == 0 ? SizedBox(width: 0.0,height: 0.0,):
                    Text(model.cartItems!.firstWhere((d) => d.productId == widget.product.id).quantity.toString(),style: theme.textTheme.bodyLarge),
                    IconButton(
                      icon: Icon(Icons.remove_circle_outline,size: 20.0.r,),
                      color: theme.colorScheme.secondary,
                      onPressed: () async{
                        await model.removeSingleItem(widget.product.id!);
                        model.getNewCartItem();
                        // if(newCartItem.length>0) {
                        //   newCartItem = Map.fromIterable(model.cartItems!, key: (v) => v.productId, value: (v) => v.quantity);
                        // }
                        // else{
                        //   newCartItem = {};
                        // }
                        await model.getTotalAmount();
                        print('item count from minus button ' + model.cartItems!.length.toString());
                        print('total amount from minus button ' + model.totalAmount.toString());
                      },
                    ),
                  ],
                )))

                :Center(child:Container(
                padding: EdgeInsets.zero,
                width: 100.w,
                height: 35.h,
                decoration: BoxDecoration(
                    color: theme.colorScheme.primary,
                    borderRadius: BorderRadius.all(Radius.circular(5.0.r))
                ),
                child: InkWell(
                  child: Center(child:Text('Add to cart',style: theme.textTheme.bodyLarge,)),
                  onTap: () async{
                    // await model.addItem(
                    //     widget.product.id,
                    //     widget.product.title,
                    //     widget.product.unit,
                    //     double.parse(widget.product.quantityLeft),
                    //     widget.product.price,
                    //     // product.discount,
                    //     0.0,
                    //     widget.product.discountId,
                    //     widget.product.discountType,
                    //     widget.product.perUnitDiscount,
                    //     widget.product.vatRate
                    // );
                    model.getNewCartItem();
                    await model.getTotalAmount();
                    print('item count from add widget ' + model.cartItems!.length.toString());
                    print('total amount from add widget ' + model.totalAmount.toString());

                  },
                )))
          ],
        ),

    );
  }

}