import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

import '../base_text_theme.dart';

class TextThemeDark implements BaseTextTheme {

  final Color? primaryColor;

  @override
  late final TextTheme data;

  @override
  String? fontFamily;

  @override
  TextStyle? bodyLarge=TextStyle(
    color: Colors.black,
    fontSize: 12.sp,
    height: 0.5.h,
  );

  @override
  TextStyle? bodyMedium=TextStyle(
    color: Colors.white,
    fontSize: 10.sp,
    height: 0.5.h,
  );

  @override
  TextStyle? bodySmall=TextStyle(
    color:  Colors.red,
    fontSize: 8.sp,
    height: 0.5.h,
  );

  @override
  TextStyle? displayLarge=TextStyle(
    color:  Colors.black,
    fontSize: 18.sp,
    height: 0.5.h,
  );

  @override
  TextStyle? displayMedium=TextStyle(
    color: Colors.white,
    fontSize: 16.sp,
    height: 0.5.h,
  );

  @override
  TextStyle? displaySmall=TextStyle(
    color:  Colors.black,
    fontSize: 14.sp,
    height: 0.5.h,
  );


  @override
  TextStyle? headlineLarge=TextStyle(
    color:  Colors.black,
    fontSize: 24.sp,
    height: 1.h,
  );

  @override
  TextStyle? headlineMedium=TextStyle(
    color:  Colors.black,
    fontSize: 22.sp,
    height: 1.h,
  );

  @override
  TextStyle? headlineSmall=TextStyle(
    color:  Colors.black,
    fontSize: 20.sp,
    height: 1.h,
  );

  @override
  TextStyle? labelLarge=TextStyle(
    color: Colors.black,
    fontSize: 10.sp,
  );

  @override
  TextStyle? labelMedium=TextStyle(
    color: Colors.black,
    fontSize: 8.sp,
  );

  @override
  TextStyle? labelSmall= TextStyle(
    color: Colors.grey,
    fontSize: 8.sp,
  );

  @override
  TextStyle? titleLarge = TextStyle(
    color:  Colors.black,
    fontSize: 20.sp,
    height: 1.5.h,
    fontWeight: FontWeight.bold,
  );

  @override
  TextStyle? titleMedium=TextStyle(
    color:  Colors.black,
    fontSize: 28.sp,
    height: 1.5.h,
  );

  @override
  TextStyle? titleSmall=TextStyle(
    color: Colors.black,
    fontSize: 26.sp,
    height: 1.5.h,
  );


  TextThemeDark(this.primaryColor) {
    data = TextTheme(
      titleLarge: titleLarge,
      titleMedium: titleMedium,
      titleSmall: titleSmall,
      bodyLarge: bodyLarge,
      bodyMedium: bodyMedium,
      bodySmall: bodySmall,
      displayLarge: displayLarge,
      displayMedium: displayMedium,
      displaySmall: displaySmall,
      headlineLarge: headlineLarge,
      headlineMedium: headlineMedium,
      headlineSmall: headlineSmall,
      labelLarge: labelLarge,
      labelMedium: labelMedium,
      labelSmall: labelSmall,

    ).apply(bodyColor: primaryColor);

    fontFamily = GoogleFonts.arvo().fontFamily;
  }








  // @override
  // TextStyle? bodyText1;
  //
  // @override
  // TextStyle? bodyText2;
  //
  // @override
  // TextStyle? headline1;
  //
  // @override
  // TextStyle? headline3;
  //
  // @override
  // TextStyle? headline4;
  //
  // @override
  // TextStyle? headline5;
  //
  // @override
  // TextStyle? headline6;
  //
  // @override
  // TextStyle? subtitle1;
  //
  // @override
  // TextStyle? subtitle2;
  //
  // @override
  // String? fontFamily;
}
