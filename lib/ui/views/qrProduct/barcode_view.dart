import 'dart:async';

import 'package:delivery_app/app/app_local.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/ui/partialViews/cart/cartButton.dart';
import 'package:delivery_app/ui/partialViews/cart/cartViewModel.dart';
import 'package:delivery_app/ui/partialViews/cart/subTotalWidget.dart';
import 'package:delivery_app/ui/sharedWidgets/app_image_widget.dart';
import 'package:delivery_app/ui/sharedWidgets/app_list_view.dart';
import 'package:delivery_app/ui/views/productList/product_viewModel.dart';
import 'package:delivery_app/ui/widgets/app_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:stacked/stacked.dart';



class BarcodeView extends StatefulWidget {
  @override
  _BarcodeViewState createState() => _BarcodeViewState();
}

class _BarcodeViewState extends State<BarcodeView> {
  String _scanBarcode = 'Unknown';
  ProductViewModel _productViewModel = locator<ProductViewModel>();

  @override
  void initState() {
    super.initState();
  }

  Future<void> startBarcodeScanStream() async {
    FlutterBarcodeScanner.getBarcodeStreamReceiver(
        '#ff6666', 'Cancel', true, ScanMode.BARCODE)!
        .listen((barcode) => print(barcode));
  }

  Future<void> scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.QR);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> scanBarcodeNormal(CartViewModel model) async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode('#ff6666', 'Cancel', true, ScanMode.BARCODE);
      print(barcodeScanRes);
      await _productViewModel.fetchProduct(barcodeScanRes);
      if(_productViewModel.product != null){
        await model.addItem(
            _productViewModel.product!.id,
            _productViewModel.product!.title,
            _productViewModel.product!.unit,
            double.parse(_productViewModel.product!.quantityLeft),
            _productViewModel.product!.price,
            _productViewModel.product!.avgUnitCost,
            _productViewModel.product!.isNonInventory,
            _productViewModel.product!.salesAccountsGroupId,
            // product.discount,
            0.0,
            _productViewModel.product!.discountId,
            _productViewModel.product!.discountType,
            _productViewModel.product!.perUnitDiscount,
            _productViewModel.product!.vatRate
        );
        model.getNewCartItem();
        await model.getTotalAmount();
        print('item count from add widget ' + model.cartItems.length.toString());
        print('total amount from add widget ' + model.totalAmount.toString());
      }
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });
  }


  final RefreshController refreshController = RefreshController();
  static ScrollController scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<CartViewModel>.reactive(
        viewModelBuilder: () => CartViewModel(),
        onViewModelReady: (model) async{
        },
        builder: (context, model, child) => Scaffold(
          backgroundColor: theme.colorScheme.background,
          appBar: AppBar(
            title: Text(AppLocale.productList.getString(context),style:theme.textTheme.titleLarge),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.scanner_sharp),
                onPressed: () {
                  scanBarcodeNormal(model);
                },
              ),
            ],
          ),
          drawer: AppDrawer(),
          body:

          model.isBusy ? Center(child: CircularProgressIndicator()) :

          Container(
            child: Column(
              children: <Widget>[
                // TestWidget(),
                Expanded(
                    child:
                    Container(child: model.cartItems != null && model.cartItems.length > 0 ?

                    //     ListView.builder(
                    //         itemCount: model.cartItems.length,
                    //         itemBuilder: (context,index) =>
                    //             ListTile(
                    //               title: Text(model.cartItems[index].title!,style: theme.textTheme.headlineSmall,),
                    //               subtitle: Text('price : ' + model.cartItems[index].price.toString()),
                    //               trailing: Container(width: 120.w,height: 80.h,child:CartButton(product:model.cartItems[index])),
                    //             )
                    // )

                    Column(
                        children: <Widget>[
                          SizedBox(height: 5.h),
                          Expanded(
                            child: ListView.builder(
                              itemCount: model.cartItems.length,
                              itemBuilder: (context,i){
                                return Dismissible(
                                    key:UniqueKey(),
                                    direction: DismissDirection.endToStart,
                                    background: Container(
                                      color: Theme.of(context).errorColor,
                                      child: Icon(Icons.delete,color: Colors.white,size: 40,),
                                      alignment: Alignment.centerRight,
                                      padding: EdgeInsets.only(right: 20),
                                      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
                                    ),
                                    confirmDismiss: (direction){
                                      return showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (context) => AlertDialog(
                                            title: Text('Remove confirmation',textAlign: TextAlign.center,style: Theme.of(context).textTheme.headline1,),
                                            content: Text('Do you want to remove this item?',textAlign: TextAlign.center,),
                                            actions: <Widget>[
                                              TextButton(child: Text('No'), onPressed: (){Navigator.of(context).pop(false);},),
                                              TextButton(child: Text('Yes'), onPressed: (){Navigator.of(context).pop(true);},),
                                            ],
                                          )
                                      );
                                    },
                                    onDismissed: (direction) async{
                                      await model.removeCartItemRow(model.cartItems[i].productId!);
                                      await model.fetchAndSetCartItems();
                                      await model.getTotalAmount();
                                    },
                                    child: Card(
                                      margin: EdgeInsets.all(2.w),
                                      child: Column(
                                        children: <Widget>[
                                          ListTile(

                                            title: Text(model.cartItems[i].title!,style: Theme.of(context).textTheme.headline3,),
                                            subtitle: Text('Total : ' + (model.cartItems[i].price.toDouble() * model.cartItems[i].quantity).toStringAsFixed(2) + ' BDT'),
                                            trailing: Container(
                                              width: 120.w,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  IconButton(
                                                    icon: Icon(Icons.add_circle_outline,size: 20.w,),
                                                    color:Theme.of(context).primaryColor,
                                                    onPressed: () async{
                                                      model.addItem(
                                                          model.cartItems[i].id!,
                                                          model.cartItems[i].title!,
                                                          // model.cartItems[i].unit!,
                                                          '',
                                                          // double.parse(model.cartItems[i].quantityLeft!),
                                                          0.0,
                                                          model.cartItems[i].price,
                                                          model.cartItems[i].avgUnitCost!,
                                                          model.cartItems[i].isNonInventory!,
                                                          model.cartItems[i].salesAccountsGroupId!,
                                                          // product.discount,
                                                          0.0,
                                                          model.cartItems[i].discountId!,
                                                          model.cartItems[i].discountType!,
                                                          model.cartItems[i].perUnitDiscount!,
                                                          model.cartItems[i].vatRate!
                                                      );
                                                      model.fetchAndSetCartItems();
                                                      model.getTotalAmount();
                                                    },
                                                  ),
                                                  Text(model.cartItems[i].quantity.toString(),
                                                    style: TextStyle(fontSize: 15.sp,fontWeight: FontWeight.bold,color:Theme.of(context).primaryColor,height: 1.4),
                                                  ),
                                                  IconButton(
                                                    icon: Icon(Icons.remove_circle_outline,size: 20.w,),
                                                    color: Theme.of(context).primaryColor,
                                                    onPressed: (){
                                                      model.removeSingleItem(model.cartItems[i].productId!);
                                                      model.fetchAndSetCartItems();
                                                      model.getTotalAmount();
                                                    },
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                );
                              },
                            ),
                          ),
                        ]
                    )
                    :Center(child: Text('no product added yet')),
                      //// ListViewWidget(model:model,products:model.products,loadMore: _loadMoreProducts,): Center(child: Text('No item found',style: Theme.of(context).textTheme.subtitle1),)
                    )
                ),
                SubtotalWidget()
              ],
            ),
          ),

        )
    );

  }

  // @override
  // Widget build(BuildContext context) {
  //   return MaterialApp(
  //       home: Scaffold(
  //           appBar: AppBar(
  //             title: const Text('Barcode scan'),
  //             actions: <Widget>[
  //               IconButton(
  //                 icon: Icon(Icons.scanner_sharp),
  //                 onPressed: () {
  //                   scanBarcodeNormal();
  //                 },
  //               ),
  //             ],
  //           ),
  //           body: Builder(builder: (BuildContext context) {
  //             return Container(
  //                 alignment: Alignment.center,
  //                 child: Flex(
  //                     direction: Axis.vertical,
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     children: <Widget>[
  //                       ElevatedButton(
  //                           onPressed: () => scanBarcodeNormal(),
  //                           child: Text('Start barcode scan')),
  //                       ElevatedButton(
  //                           onPressed: () => scanQR(),
  //                           child: Text('Start QR scan')),
  //                       ElevatedButton(
  //                           onPressed: () => startBarcodeScanStream(),
  //                           child: Text('Start barcode scan stream')),
  //                       Text('Scan result : $_scanBarcode\n',
  //                           style: TextStyle(fontSize: 20))
  //                     ]));
  //           })));
  // }
}
