

import 'package:delivery_app/data/dataHelper/app_prefs.dart';
import 'package:delivery_app/data/services/apiServices/local_data/cartService.dart';
import 'package:delivery_app/data/services/apiServices/remote_data/authService.dart';
import 'package:delivery_app/data/services/apiServices/remote_data/branchService.dart';
import 'package:delivery_app/data/services/apiServices/remote_data/orderService.dart';
import 'package:delivery_app/data/services/apiServices/remote_data/productService.dart';
import 'package:delivery_app/data/services/helperServices/dialog_service.dart';
import 'package:delivery_app/data/services/helperServices/dio_service.dart';
import 'package:delivery_app/data/services/helperServices/navigation_service.dart';
import 'package:delivery_app/domain/services/network_status_service.dart';
import 'package:delivery_app/domain/viewModels/appBase_viewModel.dart';
import 'package:delivery_app/domain/viewModels/auth_viewModel.dart';
import 'package:delivery_app/ui/partialViews/cart/cartViewModel.dart';
import 'package:delivery_app/ui/views/login/login_viewModel.dart';
import 'package:delivery_app/ui/views/orderList/order_viewModel.dart';
import 'package:delivery_app/ui/views/productList/product_viewModel.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stacked_themes/stacked_themes.dart';




GetIt locator = GetIt.instance;

Future<void> setupLocator() async{

  final sharedPreferences = await SharedPreferences.getInstance();
  locator.registerLazySingleton<SharedPreferences>(() => sharedPreferences);
  locator.registerLazySingleton<CustomDialogService>(() => CustomDialogService());
  locator.registerLazySingleton<CustomNavigationService>(() => CustomNavigationService());

  locator.registerLazySingleton<AppPreference>(() => AppPreference(locator()));
  locator.registerLazySingleton<AppBaseViewModel>(() => AppBaseViewModel());
  locator.registerLazySingleton<AuthViewModel>(() => AuthViewModel());
  locator.registerLazySingleton<LoginViewModel>(() => LoginViewModel());
  locator.registerLazySingleton<ProductViewModel>(() => ProductViewModel());
  locator.registerLazySingleton<OrderViewModel>(() => OrderViewModel());
  locator.registerFactory(() => CartViewModel());

  locator.registerLazySingleton<DioService>(() => DioService());

  locator.registerSingleton(ThemeService.getInstance());

  locator.registerLazySingleton<NetworkStatusService>(() => NetworkStatusService());
  locator.registerLazySingleton<ProductService>(() => ProductService());
  locator.registerLazySingleton<OrderService>(() => OrderService());
  locator.registerLazySingleton<BranchService>(() => BranchService());
  locator.registerLazySingleton<CartService>(() => CartService());






  locator.registerFactory(() => AuthService());



}



