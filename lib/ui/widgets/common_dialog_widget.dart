//
// import 'package:flutter/material.dart';
// import 'package:delivery_app/app/di/locator.dart';
// import 'package:delivery_app/data/services/helperServices/dialog_service.dart';
// import 'package:delivery_app/domain/models/dialog_models.dart';
//
//
// class DialogWidget extends StatefulWidget {
//   final Widget? child;
//   final BuildContext context;
//   DialogWidget({Key? key, this.child,required this.context}) : super(key: key);
//
//   _DialogWidgetState createState() => _DialogWidgetState();
// }
//
// class _DialogWidgetState extends State<DialogWidget> {
//   CustomDialogService? _dialogService = locator<CustomDialogService>();
//
//   @override
//   void initState() {
//     super.initState();
//     _dialogService!.registerDialogListener(_showDialog);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return widget.child!;
//   }
//
//   void _showDialog(DialogRequest request) {
//     var isConfirmationDialog = request.cancelTitle != null;
//     showDialog(
//         context: context,
//         // context: widget.context,
//         builder: (context) => AlertDialog(
//           title: Text(request.title),
//           content: Text(request.description),
//           actions: <Widget>[
//             if (isConfirmationDialog)
//               TextButton(
//                 child: Text(request.cancelTitle!),
//                 onPressed: () {
//                   _dialogService!
//                       .dialogComplete(DialogResponse(confirmed: false));
//                 },
//               ),
//             TextButton(
//               child: Text(request.buttonTitle),
//               onPressed: () {
//                 _dialogService!
//                     .dialogComplete(DialogResponse(confirmed: true));
//               },
//             ),
//           ],
//         ));
//   }
// }
