
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/app/app_config.dart';
import 'package:delivery_app/app/app_local.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/ui/partialViews/cart/cartButton.dart';
import 'package:delivery_app/ui/partialViews/cart/cartViewModel.dart';
import 'package:delivery_app/ui/partialViews/cart/subTotalWidget.dart';
import 'package:delivery_app/ui/sharedWidgets/app_image_widget.dart';
import 'package:delivery_app/ui/sharedWidgets/app_list_item.dart';
import 'package:delivery_app/ui/sharedWidgets/app_list_view.dart';
import 'package:delivery_app/ui/views/productList/product_viewModel.dart';
import 'package:delivery_app/ui/widgets/app_drawer.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:stacked/stacked.dart';




class ProductList extends StatefulWidget{
  @override
  _ProductListState createState() => _ProductListState();

}

class _ProductListState extends State<ProductList> {

  final RefreshController refreshController = RefreshController();
  static ScrollController scrollController = ScrollController();
  CartViewModel _cartViewModel = locator<CartViewModel>();


  _loadMoreProducts(ProductViewModel model) async{
    await model.fetchProducts();
  }



  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<ProductViewModel>.reactive(
        viewModelBuilder: () => ProductViewModel(),
        onViewModelReady: (model) async{
          await model.fetchProducts();
        },
        builder: (context, model, child) => Scaffold(
          backgroundColor: theme.colorScheme.background,
          appBar: AppBar(
            title: Text(AppLocale.productList.getString(context),style:theme.textTheme.titleLarge),
          ),
          drawer: AppDrawer(),
          body:

          model.isBusy ? Center(child: CircularProgressIndicator()) :
          model.errorMessage != null && model.errorMessage != ''? Center(child: Text(model.errorMessage!,style: TextStyle(color:Colors.red,fontWeight: FontWeight.bold),)):

          Container(
            child: Column(
              children: <Widget>[
                // TestWidget(),
                Expanded(
                    child:
                    Container(child: model.products != null && model.products.length > 0 ?
                      AppListView<ProductViewModel>(
                        refreshController: refreshController,
                        scrollController: scrollController,
                        loadMore: _loadMoreProducts,
                        model: model,
                        itemBackgroundColor: theme.colorScheme.background,
                        items: List<BaseListItem>.generate(
                          model.products.length, (i) =>
                            AppListItem(
                            // leading:'id : ${model.products[i].id}',
                            leadingWidget: AppImageWidget(
                              width: 40.w,
                              height: 40.h,
                              isRoundedCorner: true,
                              imageUrl:"${AppConfig.cdnUrl + model.products[i].imageUrl}",
                              placeholder: 'assets/images/logo.png',
                              errorImage: 'assets/images/no-image-found.png',
                            ),
                            titleWidget: Text(model.products[i].title,style: theme.textTheme.bodyLarge,),
                            subTitleWidget: Text('price :  ${model.products[i].price}',style: theme.textTheme.bodySmall,),
                            // subTitleStyle:theme.textTheme.bodySmall,
                            // trailingWidget: 'price :  ${model.products[i].price}'
                            // trailingWidget: Text('cartbutton')
                            //     trailingWidget: Container(width: 120.w,height: 80.h,child:CartButton(product:model.products[i]))
                                trailingWidget: Container(width: 120.w,height: 80.h,child:Text(''))
                        ),),
                      ):Center(child: Text('no product found')),
                      //// ListViewWidget(model:model,products:model.products,loadMore: _loadMoreProducts,): Center(child: Text('No item found',style: Theme.of(context).textTheme.subtitle1),)
                    )
                ),
                SubtotalWidget()
              ],
            ),
          ),

        )
    );

  }

}














