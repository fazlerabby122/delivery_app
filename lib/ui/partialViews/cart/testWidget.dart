
import 'package:delivery_app/ui/partialViews/cart/cartViewModel.dart';
import 'package:stacked/stacked.dart';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class TestWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return ViewModelBuilder<CartViewModel>.reactive(
     viewModelBuilder: () => CartViewModel(),
     onViewModelReady: (model) async{
       await model.getTotalAmount();
     },
       builder: (context,model,child) => Text(model.totalAmount.toStringAsFixed(2).toString(),style: theme.textTheme.bodyMedium,)
   );
  }

}