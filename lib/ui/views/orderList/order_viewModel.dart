

import 'dart:convert';
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:flutter/services.dart';
import 'package:delivery_app/app/app_config.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/data/services/apiServices/remote_data/orderService.dart';
import 'package:delivery_app/data/services/apiServices/remote_data/productService.dart';
import 'package:delivery_app/domain/models/orderItem.dart';
import 'package:delivery_app/domain/models/product_item.dart';
import 'package:delivery_app/domain/viewModels/appBase_viewModel.dart';

class OrderViewModel extends AppBaseViewModel{

  final OrderService _orderService = locator<OrderService>();

  static List<OrderItem> _orders = [];
  List<OrderItem> get orders => _orders;


  // var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZDE0NjFiYTc1ZmQ5MjlkNmFiOWU0NGViNzBjYjQxN2I3NTMxYjE5OWY5YjJlN2ZhNTZkNmU2ODFhYmExMTJmZTRlMmJlMWI5MWU0NmE3ODQiLCJpYXQiOjE2Nzc3NDk3MjYsIm5iZiI6MTY3Nzc0OTcyNiwiZXhwIjoxNzA5MzcyMTI2LCJzdWIiOiIxMSIsInNjb3BlcyI6WyIqIl19.lZtDq0B8Rm-KV_0pdrGkqIf-eO07Csb6I2uE-Fbc5HKbkjdCnPtXz0mbjkKzR3iQZvNuGqWAtyOmV2cGAgm9zYG4LD0Dz2v7GA3V0MQPmfcboPsSPXMtaLfbPDwIXQYh9XBkR_R6vOm-6ZWWwHhCJ5oYTerdaHJ1m4XgmAZxEvHVe7BkmDYbH5w0LCxlfr0qRlYshc7usx0A8t2FJnNMeHQF5cAzlNX2-5r0hxF2WBTVFiTKbFKm3bnkz-RT610XoNA3DPeQ-rVtxtsZV7tH2ZcP464zM_2DYyzkzsoB4ESdqfvn_MbaC4YS7l3V7qk74fuvkLn2xqXrG9t9GzBqS-9-nO9bC_qYlqVdG_c-SiJRPitQEAlno4RKG7XbmvfJbQz2DVSO0Znm1p43fDFjI0z8PDkyYBkGNScWi-g99r5yPihoab9ExMD_kRDVgGzq19BqDuw6yzazrMRsuxNCkZQBQ9T0q9grgbhn85Ud7oekv8G0XvNELRFmUSyy1ZzheuLegy3Ar_7ydEpZt7FdKLwejsWAWI32foZMX9cKS2HTttR3T4XNSbRNmXirFW-SVoETJPVUCyZk8dpCFCvoXsPIjx9XSiOOijPEgJ1iPEtQBLn3GPg5PjmYlB9r7SAqocANkOBit8HhT1iCghYe0bQVBhYB0K_RdyLd0djWhW8";

  Future<void> fetchAndSetOrders() async {
    refreshPageCount();
    var qString = "${AppConfig.baseUrl + AppConfig.orderFetchApi}?page_size=$perPageItem&page=$pageCount&status_array[0]=0&invoice_from_date=2022-04-02 00:00:00.000&invoice_to_date=2023-02-17 17:08:30.959850";
    try {
      setBusy(true);
      var data = await _orderService.loadOrders(qString,loggedInUser!.userToken);
      setBusy(false);
      data.fold(
              (failure){
            setErrorMessage(failure.message);
          },(data){
        var orderData = data['data'];
        processData(orderData);
      }
      );

      notifyListeners();
    }
    catch (error) {
      throw (error);
    }
  }


  processData(var extractedData){
    final List<OrderItem> loadedOrders = [];
    var allOrders = extractedData['data']['data'];
    setLastPageNo(extractedData['data']['last_page']);
    setOldPageCount(0);

    for (int i = 0; i < allOrders.length; i++) {
      final OrderItem order = OrderItem.fromJson(allOrders[i]);
      loadedOrders.add(order);
    }
    _orders = loadedOrders;
    notifyListeners();
  }

  Future<void> fetchAndSetMoreOrders() async {
    var qString = "${AppConfig.baseUrl + AppConfig.orderFetchApi}?page_size=$perPageItem&page=$pageCount&status_array[0]=0&invoice_from_date=2022-04-02 00:00:00.000&invoice_to_date=2023-02-17 17:08:30.959850";

    if(pageCount - oldPageCount == 1  && !isLastPage) {
      print('more item');
      incrementPageCount();
      var extractedData = await _orderService.loadOrders(qString,loggedInUser!.userToken);

      extractedData.fold(
              (failure){
            setErrorMessage(failure.message);
          },(data){
        final List<OrderItem> loadedOrders = [];
        List<OrderItem> newLoadedOrders = [];
        var productData = data['data'];

        var allProduct = productData['data']['data'];
        setLastPageNo(productData['data']['last_page']);
        incrementOldPageCount();
        // _oldPageCount += 1;
        for (int i = 0; i < allProduct.length; i++) {
          final OrderItem product = OrderItem.fromJson(allProduct[i]);
          loadedOrders.add(product);
        }
        newLoadedOrders = loadedOrders;
        _orders.addAll(newLoadedOrders);
        print('Orders count : ' + _orders.length.toString());
      }
      );
      notifyListeners();
    }
  }
}