import 'dart:ui';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/app/styles/themes/colors/app_colors.dart';

part './themeColors/light_color.dart';
part './themeColors/dark_color.dart';


abstract class BaseColorTheme {
  AppColors get colors;
  Color? backgroundColor;
  Color? primaryColor;
  Color? scaffoldBackgroundColor;
  Color? appBarColor;
  Color? tabBarColor;
  Color? tabbarSelectedColor;
  Color? tabbarNormalColor;
  Brightness? brightness;

  ColorScheme? colorScheme;
}






// @immutable
// class _AppColors {
//   final Color white = Color(0xffffffff);
//   final Color green = Color(0xff7bed8d);
//   final Color mediumGrey = Color(0xffa6bcd0);
//   final Color mediumGreyBold = Color(0xff748a9d);
//   final Color lighterGrey = Color(0xfff0f4f8);
//   final Color lightGrey = Color(0xffdbe2ed);
//   final Color darkerGrey = Color(0xff404e5a);
//   final Color darkGrey = Color(0xff4e5d6a);
// }
