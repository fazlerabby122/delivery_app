

class AppConfig {
  // static String baseUrl = "https://api.dev.bepari.info/demo/api/V1.5/";
  static String baseUrl = "https://api.bepari.app/demo/api/V1.5/";
  static String cdnUrl = "https://cdn.bepari.app/product-catalog-images/product/demo/";


  //auth module
  static String loginApi = "access-control/user/login";

  //product module
  static String productFetchApi = "product-catalog/product/dropdown-transaction-sales-app";

  static String productDetailApi = "https://api.dev.bepari.info/demo/api/V1.1/product-catalog/product/view-barcode";

  //order module
  static String orderFetchApi = "accounts/invoice/list";

  //branch module
  static String branchFetchApi = "access-control/user/dropdown-branches-list";
}