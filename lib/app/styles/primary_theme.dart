//
//
//
// import 'package:flutter/material.dart';
// import 'package:flutter/src/material/text_theme.dart';
// import 'package:flutter/src/painting/text_style.dart';
// import 'package:delivery_app/app/styles/themes/base_theme.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
//
// class PrimaryTheme extends BaseTheme{
//
//   @override
//   // TODO: implement titleLarge
//   TextStyle? get titleLarge => TextStyle(
//     color:  Colors.black,
//     fontSize: 30.sp,
//     height: 1.5.h,
//     fontWeight: FontWeight.bold,
//   );
//
//   @override
//   // TODO: implement titleMedium
//   TextStyle? get titleMedium => TextStyle(
//     color:  Colors.black,
//     fontSize: 28.sp,
//     height: 1.5.h,
//   );
//
//   @override
//   // TODO: implement titleSmall
//   TextStyle? get titleSmall => TextStyle(
//     color: Colors.black,
//     fontSize: 26.sp,
//     height: 1.5.h,
//   );
//
//   @override
//   // TODO: implement headlineLarge
//   TextStyle? get headlineLarge => TextStyle(
//     color:  Colors.black,
//     fontSize: 24.sp,
//     height: 1.h,
//   );
//
//   @override
//   // TODO: implement headlineMedium
//   TextStyle? get headlineMedium => TextStyle(
//     color:  Colors.black,
//     fontSize: 22.sp,
//     height: 1.h,
//   );
//
//   @override
//   // TODO: implement headlineSmall
//   TextStyle? get headlineSmall => TextStyle(
//     color:  Colors.black,
//     fontSize: 20.sp,
//     height: 1.h,
//   );
//
//   @override
//   // TODO: implement displayLarge
//   TextStyle? get displayLarge => TextStyle(
//     color:  Colors.black,
//     fontSize: 18.sp,
//     height: 0.5.h,
//   );
//
//   @override
//   // TODO: implement displayMedium
//   TextStyle? get displayMedium => TextStyle(
//     color: Colors.white,
//     fontSize: 16.sp,
//     height: 0.5.h,
//   );
//
//   @override
//   // TODO: implement displaySmall
//   TextStyle? get displaySmall => TextStyle(
//     color:  Colors.black,
//     fontSize: 14.sp,
//     height: 0.5.h,
//   );
//
//   @override
//   // TODO: implement bodyLarge
//   TextStyle? get bodyLarge => TextStyle(
//     color: Colors.black,
//     fontSize: 12.sp,
//     height: 0.5.h,
//   );
//
//   @override
//   // TODO: implement bodyMedium
//   TextStyle? get bodyMedium => TextStyle(
//     color: Colors.white,
//     fontSize: 10.sp,
//     height: 0.5.h,
//   );
//
//   @override
//   // TODO: implement bodySmall
//   TextStyle? get bodySmall => TextStyle(
//     color:  Colors.red,
//     fontSize: 8.sp,
//     height: 0.5.h,
//   );
//
//   @override
//   // TODO: implement labelLarge
//   TextStyle? get labelLarge => TextStyle(
//     color: Colors.black,
//     fontSize: 10.sp,
//   );
//
//   @override
//   // TODO: implement labelMedium
//   TextStyle? get labelMedium => TextStyle(
//     color: Colors.black,
//     fontSize: 8.sp,
//   );
//
//   @override
//   // TODO: implement labelSmall
//   TextStyle? get labelSmall => TextStyle(
//     color: Colors.grey,
//     fontSize: 8.sp,
//   );
//
//
//
//   @override
//   // TODO: implement customTextTheme
//   TextTheme get customTextTheme => TextTheme(
//     titleLarge: titleLarge,
//     titleMedium: titleMedium,
//     titleSmall: titleSmall,
//
//     headlineLarge: headlineLarge,
//     headlineMedium: headlineMedium,
//     headlineSmall: headlineSmall,
//
//     displayLarge: displayLarge,
//     displayMedium: displayMedium,
//     displaySmall: displaySmall,
//
//     bodyLarge: bodyLarge,
//     bodyMedium: bodyMedium,
//     bodySmall: bodySmall,
//
//     labelLarge: labelLarge,
//     labelMedium: labelMedium,
//     labelSmall: labelSmall,
//   );
//
//   static testTheme() {
//     return ThemeData(
//
//     );
//   }
//
//
//
//
//
// }