


import 'package:dartz/dartz.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/data/dataHelper/error_handler.dart';
import 'package:delivery_app/data/dataHelper/failure.dart';
import 'package:delivery_app/data/services/helperServices/dio_service.dart';

class ProductService {


  final DioService _dioFactory = locator<DioService>();


  Future<dynamic> loadProducts(var qString,var token) async {
    print(qString);

    Map<String,dynamic> responseData= {};

    try {
      final response = await _dioFactory.getDioWithToken(qString,token);
      if(response.statusCode == ResponseCode.SUCCESS) {
        responseData.putIfAbsent('data', () => response.data);
        return Right(responseData);
      }else{
        return Left(FailureItem(
            response.statusCode ?? 404,
            response.statusMessage ?? ResponseMessage.DEFAULT)
        );
      }
    } catch (error) {
      return (Left(ErrorHandler.handle(error).failure));
    }
  }


  Future<dynamic> loadProduct(var qString,var token) async {
    print(qString);

    Map<String,dynamic> responseData= {};

    try {
      final response = await _dioFactory.getDioWithToken(qString,token);
      if(response.statusCode == ResponseCode.SUCCESS) {
        responseData.putIfAbsent('data', () => response.data);
        return Right(responseData);
      }else{
        return Left(FailureItem(
            response.statusCode ?? 404,
            response.statusMessage ?? ResponseMessage.DEFAULT)
        );
      }
    } catch (error) {
      return (Left(ErrorHandler.handle(error).failure));
    }
  }



  Future<dynamic> searchProducts(var keyword,var qString,String token,List? inventoryType,{var setOrder = 'desc'}) async {
    if(inventoryType != null){
      var inventory = '';
      for(int i =0; i<inventoryType.length;i++){
        inventory += '&inventory_type[$i]=${inventoryType[i]}';
      }
      qString = qString + inventory;
    }

    if(keyword != null){
      qString += '&keyword=' + keyword;
    }
    Map<String,dynamic> responseData= {};

    try {
      final response = await _dioFactory.getDioWithToken(qString,token);
      if(response.statusCode == ResponseCode.SUCCESS) {
        responseData.putIfAbsent('data', () => response.data);
        return Right(responseData);
      }else{
        return Left(FailureItem(
            response.statusCode ?? 404,
            response.statusMessage ?? ResponseMessage.DEFAULT)
        );
      }
    } catch (error) {
      return (Left(ErrorHandler.handle(error).failure));
    }
  }

}
