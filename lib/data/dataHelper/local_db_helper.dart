
import 'package:delivery_app/domain/models/cartItem.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;
import 'package:sqflite/sqlite_api.dart';



class DBHelper {

  static Future<Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    // bool isExist = await sql.databaseExists(dbPath);
    // if(isExist){
    //   sql.deleteDatabase(dbPath);
    // }
    return sql.openDatabase(path.join(dbPath, 'carts.db'),
        onCreate: (db, version) {
          db.execute('CREATE TABLE cartTable('
              'id TEXT,'
              'productId TEXT,'
              'invoiceDetailsId TEXT DEFAULT NULL,'
              'title TEXT,'
              'quantity NUMERIC,'
              'stockQuantity NUMERIC,'
              'unitName TEXT,'
              'price NUMERIC,'
              'avgUnitCost NUMERIC,'
              'isNonInventory INTEGER,'
              'salesAccountsGroupId TEXT,'
              'discount NUMERIC,'
              'discountType TEXT,'
              'discountId TEXT,'
              'perUnitDiscount NUMERIC,'
              'perUnitPercentDiscount NUMERIC,'
              'perUnitOverAllDiscount NUMERIC,'
              'totalDiscount NUMERIC,'
              'overAllDiscount NUMERIC,'
              'vatRate NUMERIC,'
              'orderId TEXT)');
        }, version: 1);
  }

  static Future<bool> isProductExist(String id,String tableName) async {
    final db = await DBHelper.database();
    var result =
        await db.rawQuery('SELECT * FROM $tableName WHERE productId = $id');

    if (result.length != 0) {
      return true;
    } else {
      return false;
    }
  }

  static Future<void> updateOrderId(String table, String orderId) async {
    final db = await DBHelper.database();
    db.rawUpdate(
        'UPDATE cartTable SET orderId = $orderId WHERE tempId = temp_id');
  }

  static Future<void> insert(String table, Map<String, Object> data) async {
    final db = await DBHelper.database();
    db.insert(
      table,
      data,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<void> createCartFromOrder(CartItem cartItem) async {
    await clearCart('cartTable');
    final db = await DBHelper.database();
    final res = await db.insert('cartTable', cartItem.toJson());
    // db.close();
    // return res;
  }



  static Future<void> updateItemQuantity(String tableName,String productId, double quantity) async {
    final db = await DBHelper.database();
    db.rawUpdate('UPDATE $tableName SET quantity = $quantity WHERE productId = $productId');
  }

  static Future<void> updateItemDetail(String tableName,String productId, double quantity,double price) async {
    final db = await DBHelper.database();
    db.rawUpdate('UPDATE $tableName SET quantity = $quantity, price = $price WHERE productId = $productId');
  }

  static Future<void> updateCartItem(String productId, double quantity,double totalDiscount, double overAllDiscount) async {
    final db = await DBHelper.database();
    db.rawUpdate('UPDATE cartTable SET quantity = $quantity, totalDiscount = $totalDiscount, overAllDiscount = $overAllDiscount WHERE productId = $productId');
  }

  static Future<void> updateBillCartItem(String productId, double quantity,double price, double totalPrice) async {
    final db = await DBHelper.database();
    db.rawUpdate('UPDATE billTable SET quantity = $quantity, price = $price, totalPrice = $totalPrice WHERE productId = $productId');
  }

  static Future<void> increaseItemQuantity(String table, String productId) async {
    final db = await DBHelper.database();
    db.rawUpdate(
        'UPDATE cartTable SET quantity = quantity+1.0 WHERE productId = $productId');
  }

  static Future<void> decreaseItemQuantity(String productId) async {
    final db = await DBHelper.database();
    db.rawUpdate(
        'UPDATE cartTable SET quantity = quantity-1 WHERE productId = $productId');
  }

  static Future<void> deleteCartItm(String productId,String tableName) async {
    final db = await DBHelper.database();
    await db.rawDelete('DELETE FROM $tableName WHERE productId = $productId');
  }

  static Future<List<Map<String, dynamic>>> getData(String table) async {
    final db = await DBHelper.database();
    return await db.query(table);
  }


  static Future<CartItem?> getSingleData(String productId) async {
    final db = await DBHelper.database();
    final result = await db.rawQuery('SELECT * FROM cartTable WHERE productId = $productId');
    if (result.length > 0) {
      return await new CartItem.fromJson(result.first);
    }
    return null;
  }



  static Future<void> clearCart(String tableName) async {
    final db = await DBHelper.database();
    await db.rawQuery('DELETE  FROM $tableName');
//    db.rawDelete('DELETE * from cartTable ');
  }
}
