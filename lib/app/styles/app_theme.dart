// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
//
// class AppTheme {
//
//   static themePrimary() {
//
//     final TextStyle _titleLarge = TextStyle(
//       color:  Colors.black,
//       fontSize: 30.sp,
//       height: 1.5.h,
//       fontWeight: FontWeight.bold,
//     );
//
//     final TextStyle _titleMedium = TextStyle(
//       color:  Colors.black,
//       fontSize: 28.sp,
//       height: 1.5.h,
//     );
//
//     final TextStyle _titleSmall = TextStyle(
//       color: Colors.black,
//       fontSize: 26.sp,
//       height: 1.5.h,
//     );
//
//     final TextStyle _headlineLarge = TextStyle(
//       color:  Colors.black,
//       fontSize: 24.sp,
//       height: 1.h,
//     );
//
//     final TextStyle _headlineMedium = TextStyle(
//       color:  Colors.black,
//       fontSize: 22.sp,
//       height: 1.h,
//     );
//
//     final TextStyle _headlineSmall = TextStyle(
//       color:  Colors.black,
//       fontSize: 20.sp,
//       height: 1.h,
//     );
//
//     final TextStyle _displayLarge = TextStyle(
//       color:  Colors.black,
//       fontSize: 18.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _displayMedium = TextStyle(
//       color: Colors.white,
//       fontSize: 16.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _displaySmall = TextStyle(
//       color:  Colors.black,
//       fontSize: 14.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _bodyLarge = TextStyle(
//       color: Colors.black,
//       fontSize: 12.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _bodyMedium = TextStyle(
//       color: Colors.white,
//       fontSize: 10.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _bodySmall = TextStyle(
//       color:  Colors.red,
//       fontSize: 8.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _labelLarge = TextStyle(
//       color: Colors.black,
//       fontSize: 10.sp,
//     );
//
//     final TextStyle _labelMeidum = TextStyle(
//       color: Colors.black,
//       fontSize: 8.sp,
//     );
//
//     final TextStyle _labelSmall = TextStyle(
//       color: Colors.grey,
//       fontSize: 8.sp,
//     );
//
//
//
//     final TextTheme CustomeTextTheme = TextTheme(
//       titleLarge: _titleLarge,
//       titleMedium: _titleMedium,
//       titleSmall: _titleSmall,
//
//       headlineLarge: _headlineLarge,
//       headlineMedium: _headlineMedium,
//       headlineSmall: _headlineSmall,
//
//       displayLarge: _displayLarge,
//       displayMedium: _displayMedium,
//       displaySmall: _displaySmall,
//
//       bodyLarge: _bodyLarge,
//       bodyMedium: _bodyMedium,
//       bodySmall: _bodySmall,
//
//       labelLarge: _labelLarge,
//       labelMedium: _labelMeidum,
//       labelSmall: _labelSmall,
//     );
//
//
//     return ThemeData(
//       primaryColor:  Colors.deepOrange,
//       // accentColor:  Colors.orangeAccent,
//       // backgroundColor: Colors.grey,
//       colorScheme: ColorScheme.fromSwatch()
//           .copyWith(secondary: Colors.orangeAccent)
//           .copyWith(background: Colors.grey[300])
//           .copyWith(error: Colors.red),
//       highlightColor:  Colors.red,
//       secondaryHeaderColor:  Colors.white,
//       fontFamily: 'Josephin Sans',
//       appBarTheme: AppBarTheme(
//         backgroundColor:  Colors.teal,
//         titleTextStyle: TextStyle(color: Colors.black),
//       ),
//       iconTheme: IconThemeData(color: Colors.grey,size: 20.w),
//       primaryIconTheme: IconThemeData(color: Colors.white),
//       primaryTextTheme: CustomeTextTheme,
//       textTheme: CustomeTextTheme,
//       // accentIconTheme: IconThemeData(color:  Colors.white,),
//       // accentTextTheme: CustomeAccentTextTheme,
//       // textButtonTheme: TextButtonThemeData(
//       //   style: ButtonStyle(
//       //   backgroundColor: MaterialStateProperty.all(primaryColor != null ?Color(int.parse(primaryColor)) : Colors.teal),
//       //   textStyle: MaterialStateProperty.all(
//       //   const TextStyle(fontWeight: FontWeight.bold)),
//       //   shape: MaterialStateProperty.all(RoundedRectangleBorder(
//       //   borderRadius: BorderRadius.circular(12))),
//       //   minimumSize: MaterialStateProperty.all(const Size(400, 60)),
//       //   foregroundColor: MaterialStateProperty.all(Colors.white),
//       //   )
//       // ),
//       buttonTheme: ButtonThemeData(
//           disabledColor: Color(0xFFAFB1B3),
//           buttonColor:  Colors.teal,
//           shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0))
//       ),
//     );
//
//
//   }
//
//   static themeSecondary() {
//
//     final TextStyle _titleLarge = TextStyle(
//       color:  Colors.green,
//       fontSize: 30.sp,
//       height: 1.5.h,
//       fontWeight: FontWeight.bold,
//     );
//
//     final TextStyle _titleMedium = TextStyle(
//       color:  Colors.green,
//       fontSize: 28.sp,
//       height: 1.5.h,
//     );
//
//     final TextStyle _titleSmall = TextStyle(
//       color: Colors.green,
//       fontSize: 26.sp,
//       height: 1.5.h,
//     );
//
//     final TextStyle _headlineLarge = TextStyle(
//       color:  Colors.green,
//       fontSize: 24.sp,
//       height: 1.h,
//     );
//
//     final TextStyle _headlineMedium = TextStyle(
//       color:  Colors.green,
//       fontSize: 22.sp,
//       height: 1.h,
//     );
//
//     final TextStyle _headlineSmall = TextStyle(
//       color:  Colors.green,
//       fontSize: 20.sp,
//       height: 1.h,
//     );
//
//     final TextStyle _displayLarge = TextStyle(
//       color:  Colors.green,
//       fontSize: 18.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _displayMedium = TextStyle(
//       color: Colors.green,
//       fontSize: 16.sp,
//       height: 0.5.h,
//     );
//
//
//     final TextStyle _displaySmall = TextStyle(
//       color:  Colors.green,
//       fontSize: 14.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _bodyLarge = TextStyle(
//       color: Colors.green,
//       fontSize: 12.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _bodyMedium = TextStyle(
//       color: Colors.green,
//       fontSize: 10.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _bodySmall = TextStyle(
//       color:  Colors.green,
//       fontSize: 8.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _labelLarge = TextStyle(
//       color: Colors.green,
//       fontSize: 10.sp,
//     );
//
//     final TextStyle _labelMeidum = TextStyle(
//       color: Colors.green,
//       fontSize: 8.sp,
//     );
//
//     final TextStyle _labelSmall = TextStyle(
//       color: Colors.green,
//       fontSize: 8.sp,
//     );
//
//
//
//     final TextTheme CustomeTextTheme = TextTheme(
//       titleLarge: _titleLarge,
//       titleMedium: _titleMedium,
//       titleSmall: _titleSmall,
//
//       headlineLarge: _headlineLarge,
//       headlineMedium: _headlineMedium,
//       headlineSmall: _headlineSmall,
//
//       displayLarge: _displayLarge,
//       displayMedium: _displayMedium,
//       displaySmall: _displaySmall,
//
//       bodyLarge: _bodyLarge,
//       bodyMedium: _bodyMedium,
//       bodySmall: _bodySmall,
//
//       labelLarge: _labelLarge,
//       labelMedium: _labelMeidum,
//       labelSmall: _labelSmall,
//     );
//
//
//     return ThemeData(
//       primaryColor:  Colors.blueGrey,
//       // accentColor:  Colors.orangeAccent,
//       // backgroundColor: Colors.grey,
//       colorScheme: ColorScheme.fromSwatch()
//           .copyWith(secondary: Colors.blueAccent)
//           .copyWith(background: Colors.white)
//           .copyWith(error: Colors.red),
//       highlightColor:  Colors.red,
//       secondaryHeaderColor:  Colors.white,
//       fontFamily: 'Josephin Sans',
//       appBarTheme: AppBarTheme(
//         backgroundColor:  Colors.teal,
//         titleTextStyle: TextStyle(color: Colors.black),
//       ),
//       iconTheme: IconThemeData(color: Colors.grey,size: 12.w),
//       primaryIconTheme: IconThemeData(color:  Colors.white),
//       primaryTextTheme: CustomeTextTheme,
//       textTheme: CustomeTextTheme,
//       // accentIconTheme: IconThemeData(color:  Colors.white,),
//       // accentTextTheme: CustomeAccentTextTheme,
//       // textButtonTheme: TextButtonThemeData(
//       //   style: ButtonStyle(
//       //   backgroundColor: MaterialStateProperty.all(primaryColor != null ?Color(int.parse(primaryColor)) : Colors.teal),
//       //   textStyle: MaterialStateProperty.all(
//       //   const TextStyle(fontWeight: FontWeight.bold)),
//       //   shape: MaterialStateProperty.all(RoundedRectangleBorder(
//       //   borderRadius: BorderRadius.circular(12))),
//       //   minimumSize: MaterialStateProperty.all(const Size(400, 60)),
//       //   foregroundColor: MaterialStateProperty.all(Colors.white),
//       //   )
//       // ),
//       buttonTheme: ButtonThemeData(
//           disabledColor: Color(0xFFAFB1B3),
//           buttonColor:  Colors.teal,
//           shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0))
//       ),
//     );
//
//
//   }
//
//   static themeDark() {
//
//     final TextStyle _titleLarge = TextStyle(
//       color:  Colors.blue,
//       fontSize: 30.sp,
//       height: 1.5.h,
//       fontWeight: FontWeight.bold,
//     );
//
//     final TextStyle _titleMedium = TextStyle(
//       color:  Colors.blue,
//       fontSize: 28.sp,
//       height: 1.5.h,
//     );
//
//     final TextStyle _titleSmall = TextStyle(
//       color: Colors.blue,
//       fontSize: 26.sp,
//       height: 1.5.h,
//     );
//
//     final TextStyle _headlineLarge = TextStyle(
//       color:  Colors.blue,
//       fontSize: 24.sp,
//       height: 1.h,
//     );
//
//     final TextStyle _headlineMedium = TextStyle(
//       color:  Colors.blue,
//       fontSize: 22.sp,
//       height: 1.h,
//     );
//
//     final TextStyle _headlineSmall = TextStyle(
//       color:  Colors.blue,
//       fontSize: 20.sp,
//       height: 1.h,
//     );
//
//     final TextStyle _displayLarge = TextStyle(
//       color:  Colors.blue,
//       fontSize: 18.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _displayMedium = TextStyle(
//       color: Colors.blue,
//       fontSize: 16.sp,
//       height: 0.5.h,
//     );
//
//
//     final TextStyle _displaySmall = TextStyle(
//       color:  Colors.blue,
//       fontSize: 14.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _bodyLarge = TextStyle(
//       color: Colors.blue,
//       fontSize: 12.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _bodyMedium = TextStyle(
//       color: Colors.blue,
//       fontSize: 10.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _bodySmall = TextStyle(
//       color:  Colors.blue,
//       fontSize: 8.sp,
//       height: 0.5.h,
//     );
//
//     final TextStyle _labelLarge = TextStyle(
//       color: Colors.blue,
//       fontSize: 10.sp,
//     );
//
//     final TextStyle _labelMeidum = TextStyle(
//       color: Colors.blue,
//       fontSize: 8.sp,
//     );
//
//     final TextStyle _labelSmall = TextStyle(
//       color: Colors.blue,
//       fontSize: 8.sp,
//     );
//
//
//
//     final TextTheme CustomeTextTheme = TextTheme(
//       titleLarge: _titleLarge,
//       titleMedium: _titleMedium,
//       titleSmall: _titleSmall,
//
//       headlineLarge: _headlineLarge,
//       headlineMedium: _headlineMedium,
//       headlineSmall: _headlineSmall,
//
//       displayLarge: _displayLarge,
//       displayMedium: _displayMedium,
//       displaySmall: _displaySmall,
//
//       bodyLarge: _bodyLarge,
//       bodyMedium: _bodyMedium,
//       bodySmall: _bodySmall,
//
//       labelLarge: _labelLarge,
//       labelMedium: _labelMeidum,
//       labelSmall: _labelSmall,
//     );
//
//
//     return ThemeData(
//       primaryColor:  Colors.purple,
//       // accentColor:  Colors.orangeAccent,
//       // backgroundColor: Colors.grey,
//       colorScheme: ColorScheme.fromSwatch()
//           .copyWith(secondary: Colors.purpleAccent)
//           .copyWith(background: Colors.black12)
//           .copyWith(error: Colors.red),
//       highlightColor:  Colors.red,
//       secondaryHeaderColor:  Colors.white,
//       fontFamily: 'Josephin Sans',
//       appBarTheme: AppBarTheme(
//         backgroundColor:  Colors.teal,
//         titleTextStyle: TextStyle(color: Colors.black),
//       ),
//       iconTheme: IconThemeData(color: Colors.grey,size: 12.w),
//       primaryIconTheme: IconThemeData(color:  Colors.white),
//       primaryTextTheme: CustomeTextTheme,
//       textTheme: CustomeTextTheme,
//       // accentIconTheme: IconThemeData(color:  Colors.white,),
//       // accentTextTheme: CustomeAccentTextTheme,
//       // textButtonTheme: TextButtonThemeData(
//       //   style: ButtonStyle(
//       //   backgroundColor: MaterialStateProperty.all(primaryColor != null ?Color(int.parse(primaryColor)) : Colors.teal),
//       //   textStyle: MaterialStateProperty.all(
//       //   const TextStyle(fontWeight: FontWeight.bold)),
//       //   shape: MaterialStateProperty.all(RoundedRectangleBorder(
//       //   borderRadius: BorderRadius.circular(12))),
//       //   minimumSize: MaterialStateProperty.all(const Size(400, 60)),
//       //   foregroundColor: MaterialStateProperty.all(Colors.white),
//       //   )
//       // ),
//       buttonTheme: ButtonThemeData(
//           disabledColor: Color(0xFFAFB1B3),
//           buttonColor:  Colors.teal,
//           shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0))
//       ),
//     );
//
//
//   }
//
//
//
//   // static final TextStyle _subTitle2 = TextStyle(
//   //   color: Color(0xFF757575),
//   //   fontSize: 10.0.sp,
//   // );
//   //
//   // static final TextStyle _headline6 = TextStyle(
//   //   fontSize: 20.sp,
//   //   height: 1.5,
//   //   fontWeight: FontWeight.bold,
//   // );
//   //
//   // static final TextStyle _headline4 = TextStyle(
//   //   fontSize: 18.sp,
//   //   height: 1.0,
//   //   fontWeight: FontWeight.bold,
//   //
//   // );
//   //
//   //
//   // static final TextStyle _buttonLight = TextStyle(
//   //   color: Colors.black,
//   //   fontSize: 2.5 ,
//   // );
//   //
//   // static final TextStyle _titleDark = _headline6.copyWith(color: Colors.white);
//   //
//   // static final TextStyle _subTitleDark = _subTitle2.copyWith(color: Colors.white70);
//   //
//   // static final TextStyle _buttonDark = _buttonLight.copyWith(color: Colors.black);
//   //
//   // static final TextStyle _greetingDark = _headline4.copyWith(color: Colors.black);
//   //
//   // static final TextStyle _searchDark = _searchDark.copyWith(color: Colors.black);
//   //
//   // static final TextStyle _selectedTabDark = _selectedTabDark.copyWith(color: Colors.white);
//   //
//   // static final TextStyle _unSelectedTabDark = _selectedTabDark.copyWith(color: Colors.white70);
//   //
//   // static RoundedRectangleBorder roundedBorderDecoration([double radius = 3.0]) {
//   //   return RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius));
//   // }
//
// }
//
//
//
//
//
