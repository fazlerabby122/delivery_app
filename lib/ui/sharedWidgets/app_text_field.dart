import 'package:flutter/material.dart';


class AppTextField extends StatelessWidget {
  const AppTextField({
    required this.title,
    this.hintText = '',
    this.labelText = '',
    this.suffixIcon,
    this.controller,
    this.onChanged,
    this.onTap,
    this.keyboardType = TextInputType.text,
    this.obscureText = false,
    this.preFixIcon,
    Key? key,
  }) : super(key: key);

  final String title;
  final String hintText;
  final String labelText;
  final Image? suffixIcon;
  final void Function(String)? onChanged;
  final VoidCallback? onTap;
  final TextInputType keyboardType;
  final bool obscureText;
  final Icon? preFixIcon;
  final TextEditingController? controller;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width:250,
      child:Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              title,style: Theme.of(context).textTheme.bodySmall,
            ),
          ),
          const SizedBox(height: 20),
          TextField(
            style: Theme.of(context).textTheme.bodyLarge,
            onTap: onTap,
            onChanged: onChanged,
            controller: controller,
            decoration: InputDecoration(
              prefixIcon: IconTheme(
                  data: Theme.of(context).iconTheme,
                  child: preFixIcon ?? SizedBox(width: 0.0,height: 0.0,),
                ),
              hintText: hintText,
              hintStyle:Theme.of(context).textTheme.labelSmall,
              labelText: labelText,
              labelStyle: Theme.of(context).textTheme.labelLarge
            ),
            keyboardType: keyboardType,
          ),
        ],
      )
    );
  }
}
