import 'package:flutter/material.dart';

class AppDropdown<M,VM> extends StatefulWidget {

  final VM viewModel;
  final List<M?> modelList;
  final M? value;
  final String hintText;
  final TextStyle? valueStyle;
  final Function(M?) callback;


  const AppDropdown(
      {Key? key,
        required this.viewModel,
        required this.modelList,
        required this.value,
        required this.hintText,
        required this.valueStyle,
        required this.callback})
      : super(key: key);



  @override
  State<AppDropdown<M,VM>> createState() => _AppDropdownState<M,VM>();
}

class _AppDropdownState<M,VM> extends State<AppDropdown<M,VM>> {

  @override
  Widget build(BuildContext context) {
    print(widget.modelList);
    return Container(
      color: Colors.grey,
      child: DropdownButton<M>(
        underline: const SizedBox(),
        // elevation: 0,
        isDense: false,
        isExpanded: false,
        hint:Text(widget.hintText),
        value: widget.value,
        items: widget.modelList.map((M? value) {
          return DropdownMenuItem<M>(
            value: value,
            child: Text(value.toString(),style: widget.valueStyle,),
          );
        }).toList(),
        onChanged: widget.callback,
        // onChanged: (val) {
        //   widget.callback(val);
        //   setState(() {
        //     modelItem = val;
        //   });
        // },
      ),
    );
  }
}