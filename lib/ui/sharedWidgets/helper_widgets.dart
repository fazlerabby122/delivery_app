// import 'package:flutter/material.dart';
//
//
//
// class HelperWidgets{
//
//   static Widget districtDropdown(ShippingAddress shippingAddress, Map<String, dynamic> district,Color color,var initialValue) {
//     return DropdownButtonFormField(
//       isExpanded: true,
//       decoration: InputDecoration(
//         prefixIcon: Icon(
//           Icons.location_city,
//           color: color,
//         ),
//         border: OutlineInputBorder(),
// //                enabledBorder: UnderlineInputBorder(
// //                    borderSide: BorderSide(color: Colors.white))
//       ),
//       hint: initialValue != null && initialValue != ""
//           ? Text(initialValue)
//           : Text('select city'),
//       value: shippingAddress.selectedDistrict,
//       onSaved: (dynamic value) {
//         if(shippingAddress.setSelectedDistrict == null) {
//           shippingAddress.setSelectedDistrict(initialValue);
//         }else{
//           shippingAddress.setSelectedDistrict(value);
//         }
//       },
//       validator: (dynamic value) {
//         if (shippingAddress.setSelectedDistrict == null) {
//           value = initialValue;
//         } else {
//           value = shippingAddress.setSelectedDistrict;
//         }
//         if (value == null) {
//           return 'please choose district';
//         }
//         return null;
//       },
//       onChanged: (dynamic newValue) {
//         shippingAddress.setSelectedDistrict(newValue);
//         shippingAddress.setSelectedArea(null);
//         shippingAddress.setSelectedAreaFromLocal('');
//         // setState(() {});
//       },
//       items: districtMenuItems(district),
//     );
//   }
//
//   static Widget areaDropdown(ShippingAddress shippingAddress,Map<String?, dynamic> areas,Color color,var initialValue){
//     return DropdownButtonFormField(
//       decoration: InputDecoration(
//         prefixIcon: Icon(
//           Icons.local_gas_station,
//           color: color,
//         ),
//         border: OutlineInputBorder(),
// //                enabledBorder: UnderlineInputBorder(
// //                    borderSide: BorderSide(color: Colors.white))
//       ),
//       isExpanded: true,
//       hint: shippingAddress.selectedAreaFromLocal == ''
//           ? Text('Select area')
//           : Text(shippingAddress.selectedAreaFromLocal),
//       value: shippingAddress.selectedArea,
//       onSaved: (dynamic value){
//         shippingAddress.setSelectedArea(value);
//       },
//       validator: (dynamic value) {
//         if(shippingAddress.selectedDistrict != null && shippingAddress.selectedArea == null){
//           return 'please update area';
//         }
//         if (shippingAddress.selectedArea == null) {
//           value = initialValue;
//         } else {
//           value = shippingAddress.selectedArea;
//         }
//         if (value == null) {
//           return 'please choose area';
//         }
//         return null;
//       },
//       onChanged: (dynamic newValue) {
//         shippingAddress.setSelectedArea(newValue);
//       },
//       items: areaMenuItems(areas),
//     );
//   }
//
//   static List<DropdownMenuItem> districtMenuItems(Map<String, dynamic> items) {
//     List<DropdownMenuItem> itemWidgets = [];
//     items.forEach((key, value) {
//       itemWidgets.add(DropdownMenuItem(
//         value: value,
//         child: Text(value),
//       ));
//     });
//     return itemWidgets;
//   }
//
//   static List<DropdownMenuItem> areaMenuItems(Map<String?, dynamic> items) {
//     List<DropdownMenuItem> itemWidgets = [];
//     items.forEach((key, value) {
//       itemWidgets.add(DropdownMenuItem(
//         value: key,
//         child: Text(value),
//       ));
//     });
//     return itemWidgets;
//   }
//
//   static Widget commonTextContainer(BuildContext context,TextEditingController controller,String header,TextInputType type,String? itemValue,String hintText,String errorText,int maxLine) {
//     return Container(
//         width: MediaQuery.of(context).size.width * 4/5,
//         child: Row(
//           mainAxisSize: MainAxisSize.min,
//           mainAxisAlignment: MainAxisAlignment.spaceAround,
//           children: <Widget>[
//             Container(
//               width: MediaQuery.of(context).size.width * 0.8/5,
//               child: Text(header,textAlign: TextAlign.right,style: Theme.of(context).textTheme.bodyText1,),
//             ),
//             Container(
//                 width: MediaQuery.of(context).size.width * 3/5,
//                 height: 60.0,
//                 child:
//                 TextFormField(
//                   controller: controller,
//                   keyboardType: type,
//                   maxLines: maxLine,
//                   validator: (value) {
//                     if (value!.isEmpty) {
//                       return errorText;
//                     }
//                     return null;
//                   },
//                   onSaved: (value) {
//                     itemValue = value;
//                   },
//                   decoration: InputDecoration(
//                     contentPadding: EdgeInsets.only(left:10.0),
//                     floatingLabelBehavior: FloatingLabelBehavior.never,
//                     hintText: hintText,
//                     border: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(2.0)),
//                   ),
//                 )
//             ),
//           ],
//         ));
//   }
//
//   static Widget commonTextField(BuildContext context,TextEditingController controller,TextInputType type,IconData icon,String? initialValue,String? itemValue,String hintText,String errorText,int itemValueLength,bool isMobile,int maxLine) {
//     return TextFormField(
//       controller: controller,
//       keyboardType: type,
//       maxLines: maxLine,
//       validator: (value) {
//         if (value!.isEmpty) {
//           return errorText;
//         }
//         // else if (isMobile && value.length > 11 || value.length < 11) {
//         //   return 'please provide a valid mobile number';
//         // }
//         return null;
//       },
//       onSaved: (value) {
//         itemValue = value;
//       },
//       decoration: InputDecoration(
//         prefixIcon: Icon(
//           icon,
//           color: Theme.of(context).primaryIconTheme.color,
//         ),
//         hintText: hintText,
//         border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
//       ),
//     );
//   }
//
//   static Widget buildProgressIndicator() {
//     return Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Center(
//         child: Opacity(
//           opacity: 0.0,
//           child: CircularProgressIndicator(),
//         ),
//       ),
//     );
//   }
// }
//
//
// //   static Widget districtDropdown_t(PartialViewModel shippingAddress, Map<String, dynamic> district,Color color,var initialValue) {
// //     return DropdownButtonFormField(
// //       isExpanded: true,
// //       decoration: InputDecoration(
// //         prefixIcon: Icon(
// //           Icons.location_city,
// //           color: color,
// //         ),
// //         border: OutlineInputBorder(),
// // //                enabledBorder: UnderlineInputBorder(
// // //                    borderSide: BorderSide(color: Colors.white))
// //       ),
// //       hint: initialValue != null && initialValue != ""
// //           ? Text(initialValue)
// //           : Text('select city'),
// //       value: shippingAddress.selectedDistrict,
// //       onSaved: (dynamic value) {
// //         if(shippingAddress.setSelectedDistrict == null) {
// //           shippingAddress.setSelectedDistrict(initialValue);
// //         }else{
// //           shippingAddress.setSelectedDistrict(value);
// //         }
// //       },
// //       validator: (dynamic value) {
// //         if (shippingAddress.setSelectedDistrict == null) {
// //           value = initialValue;
// //         } else {
// //           value = shippingAddress.setSelectedDistrict;
// //         }
// //         if (value == null) {
// //           return 'please choose district';
// //         }
// //         return null;
// //       },
// //       onChanged: (dynamic newValue) {
// //         shippingAddress.setSelectedDistrict(newValue);
// //         shippingAddress.setSelectedArea(null);
// //         shippingAddress.setSelectedAreaFromLocal('');
// //         // setState(() {});
// //       },
// //       items: districtMenuItems(district),
// //     );
// //   }
// //
// //   static Widget areaDropdown_t(PartialViewModel shippingAddress,Map<String?, dynamic> areas,Color color,var initialValue){
// //     return DropdownButtonFormField(
// //       decoration: InputDecoration(
// //         prefixIcon: Icon(
// //           Icons.local_gas_station,
// //           color: color,
// //         ),
// //         border: OutlineInputBorder(),
// // //                enabledBorder: UnderlineInputBorder(
// // //                    borderSide: BorderSide(color: Colors.white))
// //       ),
// //       isExpanded: true,
// //       hint: shippingAddress.selectedAreaFromLocal == ''
// //           ? Text('Select area')
// //           : Text(shippingAddress.selectedAreaFromLocal),
// //       value: shippingAddress.selectedArea,
// //       onSaved: (dynamic value){
// //         shippingAddress.setSelectedArea(value);
// //       },
// //       validator: (dynamic value) {
// //         if(shippingAddress.selectedDistrict != null && shippingAddress.selectedArea == null){
// //           return 'please update area';
// //         }
// //         if (shippingAddress.selectedArea == null) {
// //           value = initialValue;
// //         } else {
// //           value = shippingAddress.selectedArea;
// //         }
// //         if (value == null) {
// //           return 'please choose area';
// //         }
// //         return null;
// //       },
// //       onChanged: (dynamic newValue) {
// //         shippingAddress.setSelectedArea(newValue);
// //       },
// //       items: areaMenuItems(areas),
// //     );
// //   }
