import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class AppButton<T> extends StatelessWidget {

  final T model;
  final String title;
  final TextStyle? titleStyle;
  final ButtonStyle? buttonStyle;
  final Function(T) onPressed;


  const AppButton({
    required this.model,
    required this.title,
    required this.titleStyle,
    required this.buttonStyle,
    required this.onPressed,
    Key? key,
  }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return Container(
      // padding: EdgeInsets.all(20.w),
      width: 100.w,
      height: 30.h,
      child: ElevatedButton(
        style: buttonStyle,
        onPressed:(){ onPressed(model);},
        child: Text(title,style: titleStyle,),

      ),
    );
  }
}
