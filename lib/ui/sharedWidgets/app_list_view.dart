

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/ui/sharedWidgets/app_list_item.dart';
import 'package:delivery_app/ui/views/productList/product_viewModel.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class AppListView<T> extends StatelessWidget {
  T model;
  final List<BaseListItem> items;
  Color? itemBackgroundColor;
  final Function(T) loadMore;
  final RefreshController refreshController;
  final ScrollController scrollController;

  AppListView({super.key, required this.model,required this.items,this.itemBackgroundColor,required this.loadMore,required this.refreshController,required this.scrollController});

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
        enablePullUp: true,
        enablePullDown: false,
        onRefresh: () async{
          print('refreshing');
          await loadMore(model);
          refreshController.refreshCompleted();
        },
        onLoading: () async{
          print('loading');
          await loadMore(model);
          refreshController.loadComplete();
        },
        controller: refreshController,
        child: ListView.builder(
          controller: scrollController,
          itemCount: items.length,
          itemBuilder: (context, index) {
            final item = items[index];
            return Card(
                color: itemBackgroundColor,
                child:ListTile(
                leading: item.buildLeading(context),
                title: item.buildTitle(context),
                subtitle: item.buildSubtitle(context),
                trailing: item.buildTrail(context),
              ));
          },
        ),
        footer: CustomFooter(
          builder: (BuildContext context,LoadStatus? mode){
            Widget body;
            if(mode==LoadStatus.idle){
              body =  Text("No more product",style: TextStyle(color: Colors.black),);
            }
            else if(mode==LoadStatus.loading){
              body =  CupertinoActivityIndicator();
            }
            else if(mode == LoadStatus.failed){
              body = Text("Load Failed!Click retry!");
            }
            else if(mode == LoadStatus.canLoading){
              body = Text("release to load more");
            }
            else{
              body = Text("No more Product",style: TextStyle(color: Colors.black),);
            }
            return Container(
              height: 40.0.h,
              child: Center(child:body),
            );
          },
        ),
      );
  }
}