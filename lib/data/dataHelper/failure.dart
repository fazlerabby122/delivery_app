



import 'package:delivery_app/data/dataHelper/error_handler.dart';

class FailureItem {
  int code; // 200 or 400
  String message; // error or success

  FailureItem(this.code, this.message);
}

class DefaultFailure extends FailureItem {
  DefaultFailure() : super(ResponseCode.DEFAULT, ResponseMessage.DEFAULT);
}
