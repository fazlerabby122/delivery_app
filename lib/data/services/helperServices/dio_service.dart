
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

const String APPLICATION_JSON = "application/json";
const String CONTENT_TYPE = "content-type";
const String ACCEPT = "accept";
const String AUTHORIZATION = "authorization";

class DioService {

  Future<dynamic> getDioWithToken(var url,var token) async {
    Dio dio = Dio();
    int _timeOut = 60 * 1000;

    Map<String, String> headers = {
      CONTENT_TYPE: APPLICATION_JSON,
      ACCEPT: APPLICATION_JSON,
      AUTHORIZATION: 'Bearer ' + token,
    };

    dio.options = BaseOptions(
        baseUrl: url,
        // connectTimeout: _timeOut,
        // receiveTimeout: _timeOut,
        headers: headers);

    if (kReleaseMode) {
      print("release mode no logs");
    } else {
      dio.interceptors.add(PrettyDioLogger(
          requestHeader: true, requestBody: true, responseHeader: true));
    }

    final Response response = await dio.get(url);
    return response;

  }

  Future<dynamic> getDioWithOutToken(var url) async {
    Dio dio = Dio();
    int _timeOut = 60 * 1000; // 1 min
    Map<String, String> headers = {
      CONTENT_TYPE: APPLICATION_JSON,
      ACCEPT: APPLICATION_JSON,
    };

    dio.options = BaseOptions(
        baseUrl: url,
        // connectTimeout: _timeOut,
        // receiveTimeout: _timeOut,
        headers: headers);

    if (kReleaseMode) {
      print("release mode no logs");
    } else {
      dio.interceptors.add(PrettyDioLogger(
          requestHeader: true, requestBody: true, responseHeader: true));
    }

    final Response response = await dio.get(url);

    return response;
  }

  Future<dynamic> postDioWithToken({var url,var token,var data}) async {
    Dio dio = Dio();
    int _timeOut = 60 * 1000;

    Map<String, String> headers = {
      CONTENT_TYPE: APPLICATION_JSON,
      ACCEPT: APPLICATION_JSON,
      AUTHORIZATION: 'Bearer ' + token,
    };

    dio.options = BaseOptions(
        baseUrl: url,
        // connectTimeout: _timeOut,
        // receiveTimeout: _timeOut,
        headers: headers);

    if (kReleaseMode) {
      print("release mode no logs");
    } else {
      dio.interceptors.add(PrettyDioLogger(
          requestHeader: true, requestBody: true, responseHeader: true));
    }

    final Response response = await dio.post(url,data: data);
    return response;

  }

  Future<dynamic> postDioWithOutToken(var url,var data) async {
    Dio dio = Dio();
    int _timeOut = 60 * 1000; // 1 min
    Map<String, String> headers = {
      CONTENT_TYPE: APPLICATION_JSON,
      ACCEPT: APPLICATION_JSON,
    };

    dio.options = BaseOptions(
      baseUrl: url,
      // connectTimeout: _timeOut,
      // receiveTimeout: _timeOut,
      headers: headers,);

    if (kReleaseMode) {
      print("release mode no logs");
    } else {
      dio.interceptors.add(PrettyDioLogger(
          requestHeader: true, requestBody: true, responseHeader: true));
    }

    final Response response = await dio.post(url,data: data);

    return response;
  }

}



















// import 'package:bepari_sales/core/data_helper/app_prefs.dart';
// import 'package:bepari_sales/core/viewModels/baseProvider.dart';
// import 'package:bepari_sales/utils/locator.dart';
// import 'package:dio/dio.dart';
// import 'package:flutter/foundation.dart';
// import 'package:pretty_dio_logger/pretty_dio_logger.dart';
//
// const String APPLICATION_JSON = "application/json";
// const String CONTENT_TYPE = "content-type";
// const String ACCEPT = "accept";
// const String AUTHORIZATION = "authorization";
// // const String DEFAULT_LANGUAGE = "language";
//
// class DioFactory {
//   // AppPreferenceService _appPreferences;
//   // BaseProvider _baseProvider;
//   //
//   //
//   // DioFactory(this._appPreferences,this._baseProvider);
//
//   Future<Dio> getDioWithToken(var url,var token) async {
//     Dio dio = Dio();
//     int _timeOut = 60 * 1000; // 1 min
//     // String language = await _appPreferences.getAppLanguage();
//     Map<String, String> headers = {
//       CONTENT_TYPE: APPLICATION_JSON,
//       ACCEPT: APPLICATION_JSON,
//       AUTHORIZATION: 'Bearer ' + token,
//       // DEFAULT_LANGUAGE: language
//     };
//
//     dio.options = BaseOptions(
//         baseUrl: url,
//         connectTimeout: _timeOut,
//         receiveTimeout: _timeOut,
//         headers: headers);
//
//     if (kReleaseMode) {
//       print("release mode no logs");
//     } else {
//       dio.interceptors.add(PrettyDioLogger(
//           requestHeader: true, requestBody: true, responseHeader: true));
//     }
//
//     return dio;
//   }
//
//   Future<Dio> getDioWithOutToken(var url) async {
//     Dio dio = Dio();
//     int _timeOut = 60 * 1000; // 1 min
//     // String language = await _appPreferences.getAppLanguage();
//     Map<String, String> headers = {
//       CONTENT_TYPE: APPLICATION_JSON,
//       ACCEPT: APPLICATION_JSON,
//       // DEFAULT_LANGUAGE: language
//     };
//
//     dio.options = BaseOptions(
//         baseUrl: url,
//         connectTimeout: _timeOut,
//         receiveTimeout: _timeOut,
//         headers: headers);
//
//     if (kReleaseMode) {
//       print("release mode no logs");
//     } else {
//       dio.interceptors.add(PrettyDioLogger(
//           requestHeader: true, requestBody: true, responseHeader: true));
//     }
//
//     return dio;
//   }
// }
