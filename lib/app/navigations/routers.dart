
import 'package:delivery_app/ui/views/qrProduct/barcode_view.dart';
import 'package:delivery_app/ui/views/qrProduct/qr_view.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/app/navigations/route_names.dart';
import 'package:delivery_app/ui/views/login/login_view.dart';
import 'package:delivery_app/ui/views/orderList/order_list_view.dart';
import 'package:delivery_app/ui/views/productList/product_list_view.dart';
import 'package:delivery_app/ui/views/settings/settings_view.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {

    case LoginViewRoute:
      return _getPageRoute(
        routeName: settings.name!,
        viewToShow: LoginView(),
      );
    case SettingsViewRoute:
      return _getPageRoute(
        routeName: settings.name!,
        viewToShow: SettingsView(),
      );
    case QRViewRoute:
      return _getPageRoute(
        routeName: settings.name!,
        viewToShow: AppQRView(),
      );
    case BarcodeViewRoute:
      return _getPageRoute(
        routeName: settings.name!,
        viewToShow: BarcodeView(),
      );
    case ProductListRoute:
      return _getPageRoute(
        routeName: settings.name!,
        viewToShow: ProductList(),
      );
    case OrderListRoute:
      return _getPageRoute(
        routeName: settings.name!,
        viewToShow: OrderList(),
      );
    default:
      return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
                child: Text('No route defined for ${settings.name}')),
          ));
  }
}

PageRoute _getPageRoute({required String routeName, required Widget viewToShow}) {
  return MaterialPageRoute(
      settings: RouteSettings(
        name: routeName,
      ),
      builder: (_) => viewToShow);
}
