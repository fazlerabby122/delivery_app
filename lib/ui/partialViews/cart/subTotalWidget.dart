
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/data/services/helperServices/navigation_service.dart';
import 'package:delivery_app/ui/partialViews/cart/cartViewModel.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:stacked/stacked.dart';



import 'package:flutter/src/widgets/framework.dart';
import 'package:delivery_app/domain/models/product_item.dart';


class SubtotalWidget extends StatefulWidget {
  @override
  _SubtotalWidgetState createState() => _SubtotalWidgetState();
}

class _SubtotalWidgetState extends State<SubtotalWidget>{
  CustomNavigationService _navigationService = locator<CustomNavigationService>();
  final Widget goodJob = const Text('Good job!');
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CartViewModel>.reactive(
      viewModelBuilder: () => CartViewModel(),
      onViewModelReady: (model) async{
        await model.getTotalAmount();

      },
      builder: (context, model, child) =>
      model.cartItems != null && model.cartItems.length > 0 ?

      InkWell(
        child:
        Container(
            height: 50.0.h,
            color: Colors.green,
            child:Row(
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width * 5 / 7,
                    padding:
                    EdgeInsets.only(left: 20.0.w, top: 2.0.h),
                    color: Theme.of(context).accentColor,
                    child: Column(
                      mainAxisAlignment:
                      MainAxisAlignment.center,
                      children: <Widget>[
                        Center(child:Text('Subtotal: ' + model.totalAmount.toString(),style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),))
                      ],
                    )
                ),
                Container(
                  height: MediaQuery.of(context).size.height,
                  width:
                  MediaQuery.of(context).size.width * 2 / 7,
                  color: Colors.blueGrey[600],
                  child: InkWell(
                    child: Center(
                        child:
                        Icon(
                          Icons.shopping_cart,
                          size: 20.0.r,
                          color: Colors.white,
                        )

                      // Text(
                      //   'Check out',
                      //   style: TextStyle(
                      //       color: Colors.white,
                      //       fontWeight: FontWeight.bold),
                      // ),
                    ),
                    onTap: () {
                      print(model);
                    },
                  ),
                ),
              ],
            )
        ),
        onTap: () {
          print('object1');

          // _navigationService.navigateTo(CartViewRoute);
        },
      )
          : SizedBox(width: 0.0, height: 0.0,),
    );
  }

}