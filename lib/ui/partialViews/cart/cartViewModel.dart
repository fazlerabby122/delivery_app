
import 'package:flutter/material.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/data/services/apiServices/local_data/cartService.dart';
import 'package:delivery_app/domain/models/cartItem.dart';
import 'package:delivery_app/domain/viewModels/appBase_viewModel.dart';
import 'package:stacked/stacked.dart';


class CartViewModel extends ReactiveViewModel {

  final CartService _cartService = locator<CartService>();

  List<CartItem> _cartItems = [];

  List<CartItem> get cartItems => _cartService.cartItems;

  static Map<String,dynamic> _newCartItem = {};
  Map<String,dynamic> get newCartItem => _newCartItem;

  getNewCartItem(){
    if(cartItems != null) {
      _newCartItem = Map.fromIterable(
          cartItems, key: (v) => v.productId, value: (v) => v.quantity);
      notifyListeners();
    }
  }

  int get itemCount => _cartService.cartItems.length;

  static bool _isUpdateMood = false;
  bool get isUpdateMode => _isUpdateMood;

  void setUpdateMode(val){
    _isUpdateMood = val;
    notifyListeners();
  }



  static int? _invoiceIdForUpdate;
  int? get invoiceIdForUpdate => _invoiceIdForUpdate;
  void setInvoiceIdForUpdate(val){
    _invoiceIdForUpdate = val;
    notifyListeners();
  }

  static int? _salesReturnIdForUpdate;
  int? get salesReturnIdForUpdate => _salesReturnIdForUpdate;
  void setSalesReturnIdForUpdate(val){
    _salesReturnIdForUpdate = val;
    notifyListeners();
  }

  // double get totalAmount {
  //   var total = 0.0;
  //   _cartItems.forEach((item) {
  //     total += item.price.toDouble() * item.quantity;
  //   });
  //   // notifyListeners();
  //   return total;
  // }

  Future<dynamic> getTotalAmount() async{
    await  _cartService.getTotalAmount();
    notifyListeners();
  }

  double get totalAmount => _cartService.totalAmount;


  Future<void> fetchAndSetCartItems() async {
    setBusy(true);
    await _cartService.fetchAndSetCartItems();
    setBusy(false);
    notifyListeners();
  }

  Future<void> addItem(String productId, String title, String unitName,double stockQuantity, double price,double avgUnitCost, int isNonInventory, String salesAccountsGroupId, double discount, String discountId, String discountType, double perUnitDiscount, double vatRate) async {
    await _cartService.addItem(productId, title, unitName,stockQuantity, price,avgUnitCost, isNonInventory, salesAccountsGroupId, discount, discountId, discountType, perUnitDiscount, vatRate);
    await _cartService.fetchAndSetCartItems();
    notifyListeners();

  }


  Future<void> removeSingleItem(String productId) async {
    await _cartService.removeSingleItem(productId);
    await _cartService.fetchAndSetCartItems();
    if(_cartService.cartItems.length == 1 && _cartService.cartItems.first.productId == '1'){
      await _cartService.removeCartItemRow('1');
      await _cartService.fetchAndSetCartItems();
    }
    notifyListeners();
  }

  Future<void> updateSingleCartItem(String productId,double quantity) async {
    await _cartService.updateSingleCartItem(productId,quantity);
    await _cartService.fetchAndSetCartItems();
    notifyListeners();
  }

  Future<void> updateCartItem(String productId,double quantity,double totalDiscount, double overAllDiscount) async {
    await _cartService.updateCartItem(productId,quantity,totalDiscount,overAllDiscount);
    await _cartService.fetchAndSetCartItems();
    notifyListeners();
  }

  Future<void> removeCartItemRow(String productId) async {
    setBusy(true);
    await _cartService.removeCartItemRow(productId);
    await _cartService.fetchAndSetCartItems();
    setBusy(false);
    notifyListeners();
  }

  Future<void> clearCartTable() async{
    await _cartService.clearCart();
    await _cartService.fetchAndSetCartItems();
    _cartItems = [];
    notifyListeners();
  }



  void removeItem(String productId) {
//    _items.remove(productId);
//    notifyListeners();
  }

  // double getDiscount(double discount, String discountType, double unitPrice, int quantity) {
//    double discountAmount;
//    if(discount != 0.0){
//      if(discountType == 'percent'){
//        discountAmount =(discount/100);
//        discountAmount = unitPrice * discountAmount;
//      }
//      else if(discountType == 'amount'){
//        discountAmount = discount*quantity;
//      }
//    }else{
//      discountAmount = 0.0;
//    }
//    return discountAmount;
//  }
//
////  Future<void> fetchAndSetCartItems() async {
////
////    try {
////      final data = db.getAllCartItems();
////      if (data == null) {
////        return;
////      }
////      final List<CartItem> loadedProducts = [];
////
//////      _items = loadedProducts;
////      notifyListeners();
////    } catch (error) {
////      throw (error);
////    }
//   }

  @override
  List<ReactiveServiceMixin> get reactiveServices => [_cartService];
}







// class CartViewModel extends  ReactiveViewModel{
//   final CartService _cartService = locator<CartService>();
//
//   // CartViewModel(){
//   //   listenToReactiveValues([_cartItems,_totalAmount]);
//   // }
//   //
//   // final _cartItems = ReactiveValue<List<CartItem>>([]);
//   // List<CartItem> get cartItems => _cartItems.value;
//   //
//   // final _totalAmount = ReactiveValue<double>(0.0);
//   // double get totalAmount => _totalAmount.value;
//
//
//   // static List<CartItem>? _cartItems = [];
//   // List<CartItem>? get cartItems => _cartItems;
//   //
//   // static double _totalAmount =  0.0;
//   // double get totalAmount => _totalAmount;
//
//   static Map<String,dynamic> _newCartItem = {};
//   Map<String,dynamic> get newCartItem => _newCartItem;
//
//
//   List<CartItem> get cartItems => _cartService.cartItems;
//   int? get itemCount => cartItems.length;
//   double get totalAmount => _cartService.totalAmount;
//
//
//   getNewCartItem(){
//     if(cartItems != null) {
//       _newCartItem = Map.fromIterable(
//           cartItems, key: (v) => v.productId, value: (v) => v.quantity);
//       notifyListeners();
//     }
//   }
//
//   void setCartItme(List<CartItem> value){
//     cartItems.addAll(value);
//     notifyListeners();
//   }
//
//   void clearCartItem(){
//     cartItems.clear();
//     notifyListeners();
//   }
//
//   void setNewCartItme(Map<String,dynamic> value){
//     _newCartItem = value;
//     notifyListeners();
//   }
//
//   Future<void> getTotalAmount() async{
//     var total = 0.0;
//     await fetchAndSetCartItems();
//     if(cartItems != null) {
//       cartItems.forEach((item) {
//         print(
//             'name :' + item.title!
//             + '  quantity :' + item.quantity.toString()
//             + '    price :' + item.price.toString());
//         total += item.price.toDouble() * item.quantity;
//       });
//       _cartService.setTotalAmount(total);
//       print('total amount in vm :' + totalAmount.toString());
//     }
//     notifyListeners();
//   }
//
//
//
//
//
//
//
//   Future<void> fetchAndSetCartItems() async {
//     setBusy(true);
//     var cartData;
//     try {
//       var data = await _cartService.fetchAndSetCartItems();
//       setBusy(false);
//       data.fold(
//         (failure){
//           // setErrorMessage(failure.message);
//           // cartItems = [];
//           clearCartItem();
//         },
//         (data){
//           cartData = data['data'];
//         }
//       );
//       // cartItems = cartData;
//       setCartItme(cartData);
//       notifyListeners();
//     }
//     catch (error) {
//       throw (error);
//     }
//   }
//
//   Future<void> addItem(String productId, String title, String unitName,double stockQuantity, double price,double avgUnitCost, int isNonInventory, String salesAccountsGroupId, double discount, String discountId, String discountType, double perUnitDiscount, double vatRate) async {
//     await _cartService.addItem(productId, title, unitName,stockQuantity, price,avgUnitCost, isNonInventory, salesAccountsGroupId, discount, discountId, discountType, perUnitDiscount, vatRate);
//     await fetchAndSetCartItems();
//     notifyListeners();
//
//   }
//
//
//   Future<void> removeSingleItem(String productId) async {
//     await _cartService.removeSingleItem(productId);
//     await fetchAndSetCartItems();
//     if(cartItems != null && cartItems.length == 1 && cartItems.first.productId == '1'){
//       await _cartService.removeCartItemRow('1');
//       await fetchAndSetCartItems();
//       // _orderViewModel.setDeliveryCharge(null);
//     }
//     notifyListeners();
//   }
//
//   Future<void> updateSingleCartItem(String productId,double quantity) async {
//     await _cartService.updateSingleCartItem(productId,quantity);
//     await fetchAndSetCartItems();
//     notifyListeners();
//   }
//
//   Future<void> updateCartItem(String productId,double quantity,double totalDiscount, double overAllDiscount) async {
//     await _cartService.updateCartItem(productId,quantity,totalDiscount,overAllDiscount);
//     await fetchAndSetCartItems();
//     notifyListeners();
//   }
//
//   Future<void> removeCartItemRow(String productId) async {
//     setBusy(true);
//     await _cartService.removeCartItemRow(productId);
//     await fetchAndSetCartItems();
//     setBusy(false);
//     notifyListeners();
//   }
//
//   Future<void> clearCartTable() async{
//     await _cartService.clearCart();
//     await fetchAndSetCartItems();
//     // cartItems = [];
//     clearCartItem();
//     notifyListeners();
//   }
//
//
//
//   void removeItem(String productId) {
//    // _items.remove(productId);
// //    notifyListeners();
//   }
//
//   // double getDiscount(double discount, String discountType, double unitPrice, int quantity) {
// //    double discountAmount;
// //    if(discount != 0.0){
// //      if(discountType == 'percent'){
// //        discountAmount =(discount/100);
// //        discountAmount = unitPrice * discountAmount;
// //      }
// //      else if(discountType == 'amount'){
// //        discountAmount = discount*quantity;
// //      }
// //    }else{
// //      discountAmount = 0.0;
// //    }
// //    return discountAmount;
// //  }
// //
// ////  Future<void> fetchAndSetCartItems() async {
// ////
// ////    try {
// ////      final data = db.getAllCartItems();
// ////      if (data == null) {
// ////        return;
// ////      }
// ////      final List<CartItem> loadedProducts = [];
// ////
// //////      _items = loadedProducts;
// ////      notifyListeners();
// ////    } catch (error) {
// ////      throw (error);
// ////    }
// //   }
//
//   @override
//   List<ListenableServiceMixin> get listenableService => [_cartService];
// }

