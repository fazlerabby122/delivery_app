const String LoginViewRoute = 'LoginView';
const String SettingsViewRoute = 'SettingsView';
const String LanguageSettingsViewRoute = 'LanguageSettingsView';
const String HomeViewRoute = 'HomeView';
const String BranchViewRoute = 'BranchView';
const String QRViewRoute = 'QRView';
const String BarcodeViewRoute = 'BarcodeView';
const String ProductListRoute = 'ProductListView';
const String OrderListRoute = 'OrderListView';
const String AppWidgetsRoute = 'AppWigetsListView';
const String AppDialogRoute = "AppDialogView";
const String CacheImageRoute = "CacheImageView";
const String DateTimePickerRoute = "DateTimePickerView";



