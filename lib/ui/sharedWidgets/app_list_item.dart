


import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppListItem implements BaseListItem {
  // final String? leading;
  final Widget leadingWidget;
  // final String? title;
  final Widget titleWidget;
  // final TextStyle? titleStyle;
  final Widget subTitleWidget;
  // final TextStyle? subTitleStyle;
  final Widget trailingWidget;

  AppListItem({
    required this.leadingWidget,
    required this.titleWidget,
    // required this.titleStyle,
    required this.subTitleWidget,
    // required this.subTitleStyle,
    required this.trailingWidget});

  // @override
  // Widget buildLeading(BuildContext context) => Text(leading!);

  @override
  Widget buildLeading(BuildContext context) => leadingWidget;

  // @override
  // Widget buildTitle(BuildContext context) => Text(title!,style: titleStyle,);
  @override
  Widget buildTitle(BuildContext context) => titleWidget;

  // @override
  // Widget buildSubtitle(BuildContext context) => Text(subTitleWidget!,style: subTitleStyle,);
  @override
  Widget buildSubtitle(BuildContext context) => subTitleWidget;

  // @override
  // Widget buildTrail(BuildContext context) => Text(trailing!);

  @override
  Widget buildTrail(BuildContext context) => trailingWidget;
}


abstract class BaseListItem {
  Widget buildLeading(BuildContext context);
  Widget buildTitle(BuildContext context);
  Widget buildSubtitle(BuildContext context);
  Widget buildTrail(BuildContext context);
}