

import 'package:flutter/material.dart';
import 'package:delivery_app/app/app_local.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/app/navigations/route_names.dart';
import 'package:delivery_app/data/services/helperServices/navigation_service.dart';
import 'package:delivery_app/ui/sharedWidgets/app_button.dart';
import 'package:delivery_app/ui/sharedWidgets/app_text_field.dart';
import 'package:delivery_app/ui/sharedWidgets/network_aware_widget.dart';
import 'package:delivery_app/ui/views/login/login_viewModel.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:stacked/stacked.dart';

class LoginView extends StatelessWidget {

  final CustomNavigationService _navigationService = locator<CustomNavigationService>();



  loginUser(LoginViewModel model) async{
    model.setErrorMessage('');
    if(model.userEmail != null && model.userPassword != null){
      await model.loginUser(model.userEmail, model.userPassword);


    }
  }



  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<LoginViewModel>.reactive(
        viewModelBuilder: () => LoginViewModel(),
        onViewModelReady: (model) async {

        },
        builder: (context, model, child) =>


            NetworkAwareWidget(
              onlineChild: WillPopScope(
                  onWillPop: () async {
                    return true;
                  },
                  child:Scaffold(
                    body:


                    Center(child:
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AppTextField(
                          preFixIcon:const Icon(Icons.email),
                          title:'',labelText: 'email',
                          hintText: 'please enter email address',
                          keyboardType: TextInputType.emailAddress,
                          onChanged:(value){
                            model.setUserEmail(value);
                          },),
                        AppTextField(
                          preFixIcon:const Icon(Icons.password),
                          title:'',labelText: 'password',
                          hintText: 'please enter password',
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          onChanged: (value){
                            model.setUserPassword(value);
                          },
                        ),
                        SizedBox(height: 45.h,),
                        model.errorMessage != null ? Center(child: Text(model.errorMessage!,style: TextStyle(color:Colors.red,fontWeight: FontWeight.bold),)):
                        SizedBox(width: 0.0,height: 0.0,),
                        SizedBox(height: 15.h,),
                        model.isBusy ? Center(child: CircularProgressIndicator(),):
                        // Container(
                        //   // padding: EdgeInsets.all(20.w),
                        //   width: 100.w,
                        //   height: 30.h,
                        //   child: ElevatedButton(
                        //     onPressed:(){ loginUser(model);},
                        //     child: Text('login',style: theme.textTheme.bodyMedium,),
                        //
                        //   ),
                        // )
                        AppButton<LoginViewModel>(
                          model: model,
                          title: AppLocale.loginButtonText.getString(context),
                          titleStyle: theme.textTheme.bodyMedium,
                          buttonStyle: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(theme.colorScheme.secondary,),
                          ),
                          onPressed: loginUser,
                        )
                      ],
                    )),
                  )
              ),
              offlineChild: Scaffold(body:Container(
                  child:Center(
                    child: Text(AppLocale.noInternetConnection.getString(context),style: TextStyle(color: Colors.grey[400],fontSize: 20.sp,fontWeight: FontWeight.w600),),
                  )
              )),
            )

    );
  }

}