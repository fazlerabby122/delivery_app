import 'package:flutter/material.dart';

abstract class BaseTextTheme {
  // final Color? primaryColor;
  // late final TextTheme data;
  // TextStyle? headline1;
  // TextStyle? headline3;
  // TextStyle? headline4;
  // TextStyle? headline5;
  // TextStyle? headline6;
  // TextStyle? subtitle1;
  // TextStyle? subtitle2;
  // TextStyle? bodyText1;
  // TextStyle? bodyText2;
  // String? fontFamily;
  //
  // ITextTheme(this.primaryColor);


  final Color? primaryColor;
  late final TextTheme data;
  TextStyle? titleLarge;
  TextStyle? titleMedium;
  TextStyle? titleSmall;
  TextStyle? headlineLarge;
  TextStyle? headlineMedium;
  TextStyle? headlineSmall;
  TextStyle? displayLarge;
  TextStyle? displayMedium;
  TextStyle? displaySmall;
  TextStyle? bodyLarge;
  TextStyle? bodyMedium;
  TextStyle? bodySmall;
  TextStyle? labelLarge;
  TextStyle? labelMedium;
  TextStyle? labelSmall;
  String? fontFamily;

  BaseTextTheme(this.primaryColor);
}
