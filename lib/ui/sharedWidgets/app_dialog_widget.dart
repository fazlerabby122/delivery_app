
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:stacked/stacked.dart';




abstract class AbstractViewModel<VM>{
  void doStuff(dynamic value);
}


class AppDialog<VM> extends StatefulWidget {
  VM viewModel;
  final String title;
  final TextStyle? titleStyle;
  final String description;
  final TextStyle? descriptionStyle;
  Widget? customWidget;
  final String cancelButtonTitle;
  final Color cancelButtonTitleColor;
  String? confirmButtonTitle;
  Color? confirmButtonTitleColor;
  final Function() confirmButtonOnPress;
  final Function() cancelButtonOnPress;
  final Function()? notifyParent;


  AppDialog({
    required this.viewModel,
    required this.title,
    required this.titleStyle,
    required this.description,
    this.descriptionStyle,
    this.customWidget,
    required this.cancelButtonTitle,
    required this.cancelButtonTitleColor,
    this.confirmButtonTitle,
    this.confirmButtonTitleColor,
    required this.confirmButtonOnPress,
    required this.cancelButtonOnPress,
    this.notifyParent,
    Key? key,
  }): super(key: key);

  @override
  AppDialogState createState() => AppDialogState();

}

class AppDialogState extends State<AppDialog>{
  methodInChild(){
    print('start');
    widget.viewModel.doStuff;
    widget.notifyParent;
    print('refresh in child');
  }

  @override
  void didUpdateWidget(covariant  oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {});

  }



  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Center(child:Text(widget.title,style:widget.titleStyle,)),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Center(
              child: widget.customWidget != null? widget.customWidget : Text(widget.description,style: widget.descriptionStyle,),
            )
          ],
        ),

        actions: <Widget>[
          Center(child:
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              widget.confirmButtonTitle != null ? ElevatedButton(
                  child:Text(widget.confirmButtonTitle!),
                  onPressed: widget.confirmButtonOnPress
              ) :SizedBox(width: 0.0,height: 0.0,),
              widget.confirmButtonTitle != null ?SizedBox(width: 50.w,):SizedBox(width: 0.0,height: 0.0,),
              ElevatedButton(
                  child:Text(widget.cancelButtonTitle,
                    style: TextStyle(color: widget.cancelButtonTitleColor),),
                  onPressed: widget.cancelButtonOnPress
              ),
            ],
          )
          )
        ]
    );
  }


  // @override
  // Widget build(BuildContext context) {
  //   return
  //     AlertDialog(
  //       title: Center(child:Text(widget.title,style:widget.titleStyle,)),
  //       content: Column(
  //         mainAxisSize: MainAxisSize.min,
  //         children: [
  //           Center(
  //             child: widget.customWidget != null? widget.customWidget : Text(widget.description),
  //           )
  //         ],
  //       ),
  //
  //       actions: <Widget>[
  //         Center(child:
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             widget.confirmButtonTitle != null ? ElevatedButton(
  //                 child:Text(widget.confirmButtonTitle!),
  //                 onPressed: widget.confirmButtonOnPress
  //               // onPressed: methodInChild,
  //               //   // onPressed: (){
  //               //   //   print('start');
  //               //   //   widget.viewModel.doStuff;
  //               //   //   widget.notifyParent;
  //               //   // },
  //             ) :SizedBox(width: 0.0,height: 0.0,),
  //             widget.confirmButtonTitle != null ?SizedBox(width: 50.w,):SizedBox(width: 0.0,height: 0.0,),
  //             ElevatedButton(
  //                 child:Text(widget.cancelButtonTitle,
  //                 style: TextStyle(color: widget.cancelButtonTitleColor),),
  //                 onPressed: widget.cancelButtonOnPress
  //             ),
  //           ],
  //         )
  //         )
  //
  //       ]
  //   );
  // }

}


// class AppDialog<VM> extends StatefulWidget {
//   final VM viewModel;
//   final String title;
//   final TextStyle? titleStyle;
//   final String description;
//   Widget? customWidget;
//   final String cancelButtonTitle;
//   final Color cancelButtonTitleColor;
//   String? confirmButtonTitle;
//   Color? confirmButtonTitleColor;
//   final Function() confirmButtonOnPress;
//   final Function() cancelButtonOnPress;
//
//
//   AppDialog({
//     required this.viewModel,
//     required this.title,
//     required this.titleStyle,
//     required this.description,
//     this.customWidget,
//     required this.cancelButtonTitle,
//     required this.cancelButtonTitleColor,
//     this.confirmButtonTitle,
//     this.confirmButtonTitleColor,
//     required this.confirmButtonOnPress,
//     required this.cancelButtonOnPress
//   });
//
//   @override
//   _AppDialogState createState() => _AppDialogState();
//
// }
//
//
// class _AppDialogState extends State<AppDialog>{
//
//   @override
//   Widget build(BuildContext context) {
//     return AlertDialog(
//       title: Center(child:Text(widget.title,style:widget.titleStyle,)),
//         content: Column(
//           mainAxisSize: MainAxisSize.min,
//           children: [
//             Center(
//               child: widget.customWidget != null? widget.customWidget : Text(widget.description),
//             )
//           ],
//         ),
//
//         actions: <Widget>[
//
//           Center(child:
//           Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               widget.confirmButtonTitle != null ? ElevatedButton(
//                   child:Text(widget.confirmButtonTitle!),
//                   onPressed: widget.confirmButtonOnPress) :SizedBox(width: 0.0,height: 0.0,),
//               widget.confirmButtonTitle != null ?SizedBox(width: 50.w,):SizedBox(width: 0.0,height: 0.0,),
//               ElevatedButton(
//                   child:Text(widget.cancelButtonTitle,style: TextStyle(color: widget.cancelButtonTitleColor),),
//                   onPressed: widget.cancelButtonOnPress),
//             ],
//           ))
//           // if (isConfirmationDialog)
//
//         ]
//     );
//   }
//
// }
