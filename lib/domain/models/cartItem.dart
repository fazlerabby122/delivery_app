

class CartItem {
  final String? id;
  final String? productId;
  final String? invoiceDetailsId;
  final String? title;
  final String? productCategoryId;
  final double quantity;
  final double? stockQuantity;
  final String? unitName;
  final double price;
  final double? avgUnitCost;
  final int? isNonInventory;
  final String? salesAccountsGroupId;
  final double? discount;
  final String? discountType;
  final String? discountId;
  final double? perUnitDiscount;
  final double? perUnitPercentDiscount;
  final double? perUnitOverAllDiscount;
  final double? itemTotalDiscount;
  final double? overallDiscount;
  final double? vatRate;
  final String? orderId;

  CartItem({
    this.id,
    this.productId,
    this.invoiceDetailsId,
    this.title,
    this.productCategoryId,
    required this.quantity,
    this.stockQuantity,
    this.unitName,
    required this.price,
    this.avgUnitCost,
    this.isNonInventory,
    this.salesAccountsGroupId,
    this.discount,
    this.discountType,
    this.discountId,
    this.perUnitDiscount,
    this.perUnitPercentDiscount,
    this.perUnitOverAllDiscount,
    this.itemTotalDiscount,
    this.overallDiscount,
    this.vatRate,
    this.orderId,
  });


  factory CartItem.fromJson(Map<String, dynamic> data) => new CartItem(
    id: data["id"],
    productId: data["productId"],
    invoiceDetailsId: data["invoiceDetailsId"] != null ? data["invoiceDetailsId"] : '',
    title: data["title"],
    quantity: data["quantity"].toDouble(),
    stockQuantity: data["stockQuantity"] != null ? double.parse(data["stockQuantity"].toString()) :0.0,
    unitName: data["unitName"],
    price: data['price'].toDouble(),
    avgUnitCost : data['avgUnitCost'] != null ? double.parse(data['avgUnitCost'].toString()):0.0,
    isNonInventory: data['isNonInventory'],
    salesAccountsGroupId: data['salesAccountsGroupId'],
    discount: data['discount'] != null ? data['discount'].toDouble() : 0.0,
    discountType: data['discountType'],
    discountId: data['discountId'],
    perUnitDiscount: data['perUnitDiscount'] != null? data['perUnitDiscount'].toDouble():0.0,
    perUnitPercentDiscount: data['perUnitPercentDiscount'] != null? data['perUnitPercentDiscount'].toDouble():0.0,
    perUnitOverAllDiscount: data['perUnitOverAllDiscount'] != null? data['perUnitOverAllDiscount'].toDouble():0.0,
    itemTotalDiscount: data['totalDiscount'] != null? data['totalDiscount'].toDouble():0.0,
    overallDiscount: data['overAllDiscount'] != null? data['overAllDiscount'].toDouble():0.0,
    vatRate: data['vatRate'] != null ? data['vatRate'].toDouble():0.0,
    orderId: data['orderId'],
  );

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (id != null) {
      map['id'] = id;
    }
    map['productId'] = productId;
    map['invoiceDetailsId'] = invoiceDetailsId;
    map['title'] = title;
    map['quantity'] = quantity;
    map['stockQuantity'] = stockQuantity;
    map['unitName'] = unitName;
    map['price'] = price;
    map['avgUnitCost'] = avgUnitCost;
    map['isNonInventory'] = isNonInventory;
    map['salesAccountsGroupId'] = salesAccountsGroupId;
    map['discount'] = discount;
    map['discountType'] = discountType;
    map['discountId'] = discountId;
    map['perUnitDiscount'] = perUnitDiscount;
    map['perUnitPercentDiscount'] = perUnitPercentDiscount;
    map['perUnitOverAllDiscount'] = perUnitOverAllDiscount;
    map['totalDiscount'] = itemTotalDiscount;
    map['overAllDiscount'] = overallDiscount;
    map['vatRate'] = vatRate;
    map['orderId'] = orderId;
    return map;
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'productId': productId,
    'invoiceDetailsId': invoiceDetailsId,
    'title': title,
    'quantity': quantity,
    'stockQuantity' : stockQuantity,
    'unitName': unitName,
    'price': price,
    'isNonInventory': isNonInventory,
    'salesAccountsGroupId': salesAccountsGroupId,
    'discount': discount,
    'discountType': discountType,
    'discountId': discountId,
    'perUnitDiscount': perUnitDiscount,
    'perUnitPercentDiscount':perUnitPercentDiscount,
    'perUnitOverAllDiscount': perUnitOverAllDiscount,
    'totalDiscount':itemTotalDiscount,
    'overAllDiscount':overallDiscount,
    'vatRate': vatRate,
    'orderId': orderId,
  };
}