
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/domain/viewModels/appBase_viewModel.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_themes/stacked_themes.dart';


class ThemeModel {
  final int index;
  final String title;

  ThemeModel({required this.index, required this.title});
}

class SettingsViewModel extends AppBaseViewModel {
  final ThemeService _themeService = locator<ThemeService>();

  List<ThemeModel> get themes => List<ThemeModel>.generate(
      2,
      (index) => ThemeModel(
            index: index,
            title: _getTitleForIndex(index),
          ));

  String _getTitleForIndex(int index) {
    switch (index) {
      case 0:
        return 'Light theme';
      case 1:
        return 'Dark theme';
      // case 2:
      //   return 'Dark theme';
    }

    return 'No theme for index';
  }


  // void setTheme(ThemeModel themeData) {
  //   _themeService.selectThemeAtIndex(themeData.index);
  //   notifyListeners();
  // }

  void setTheme(ThemeModel themeData) =>
      _themeService.selectThemeAtIndex(themeData.index);

}
