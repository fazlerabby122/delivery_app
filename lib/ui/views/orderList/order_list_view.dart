
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/domain/models/product_item.dart';
import 'package:delivery_app/ui/sharedWidgets/app_list_item.dart';
import 'package:delivery_app/ui/sharedWidgets/app_list_view.dart';
import 'package:delivery_app/ui/views/orderList/order_viewModel.dart';
import 'package:delivery_app/ui/views/productList/product_viewModel.dart';
import 'package:delivery_app/ui/widgets/app_drawer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:stacked/stacked.dart';




class OrderList extends StatefulWidget{
  @override
  _OrderListState createState() => _OrderListState();

}

class _OrderListState extends State<OrderList> {

  final RefreshController refreshController = RefreshController();
  static ScrollController scrollController = ScrollController();

  _loadMoreOrders(OrderViewModel model) async{
    await model.fetchAndSetMoreOrders();
  }


  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<OrderViewModel>.reactive(
        viewModelBuilder: () => OrderViewModel(),
        onViewModelReady: (model) async{
          await model.fetchAndSetOrders();
        },
        builder: (context, model, child) => Scaffold(
          backgroundColor: theme.colorScheme.background,
          appBar: AppBar(title: const Text('Order list'),),
          drawer: AppDrawer(),
          body:

          model.isBusy ? const Center(child: CircularProgressIndicator()) :
          model.errorMessage != null && model.errorMessage != ''? Center(child: Text(model.errorMessage!,style: const TextStyle(color:Colors.red,fontWeight: FontWeight.bold),)):

          Container(
            child: Column(
              children: <Widget>[
                Expanded(
                    child:
                    Container(child: model.orders != null && model.orders.length > 0 ?

                      AppListView<OrderViewModel>(
                        refreshController: refreshController,
                        scrollController: scrollController,
                        loadMore: _loadMoreOrders,
                        model: model,
                        itemBackgroundColor: theme.colorScheme.background,
                        items: List<BaseListItem>.generate(
                          model.orders.length, (i) =>
                            AppListItem(
                            leadingWidget: Text('id : ${model.orders[i].id}'),
                            titleWidget: Text(model.orders[i].customerName,style: theme.textTheme.bodyLarge,),
                            subTitleWidget: Text('created at :  ${model.orders[i].createdAt}',style: theme.textTheme.bodySmall,),
                            trailingWidget: Text('invoice amount  :  ${model.orders[i].invoiceAmount}')
                        ),),
                      ):const Center(child: Text('no order found')),
                    )
                ),
              ],
            ),
          ),
        )
    );
  }

}















