


import 'dart:convert';

import 'package:delivery_app/app/app_config.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/app/navigations/route_names.dart';
import 'package:delivery_app/data/dataHelper/app_prefs.dart';
import 'package:delivery_app/data/services/apiServices/remote_data/authService.dart';

import 'package:delivery_app/data/services/helperServices/navigation_service.dart';
import 'package:delivery_app/domain/models/user_model.dart';
import 'package:delivery_app/domain/viewModels/appBase_viewModel.dart';




class LoginViewModel extends AppBaseViewModel{

  final CustomNavigationService _navigationService = locator<CustomNavigationService>();
  final AuthService _authService = locator<AuthService>();
  final AppPreference _appPreference = locator<AppPreference>();

  static String? _userEmail;
  String? get userEmail => _userEmail;

  void setUserEmail(String value){
    _userEmail = value;
    notifyListeners();
  }

  void clearUserEmail(){
    _userEmail = null;
    notifyListeners();
  }

  static String? _userPassword;
  String? get userPassword => _userPassword;

  void setUserPassword(String value){
    _userPassword = value;
    notifyListeners();
  }

  void clearUserPassword(){
    _userPassword = null;
    notifyListeners();
  }


  // Future<void> loginUser(String? email, String? password) async {
  //   var qString = "${AppConfig.baseUrl}product-catalog/product/dropdown-transaction-sales-app?page_size=$perPageItem&page=$pageCount&product_price_category_id=1&category_id=0&branch_id=1&sort_order=desc";
  //   try {
  //     setBusy(true);
  //
  //     notifyListeners();
  //   }
  //   catch (error) {
  //     throw (error);
  //   }
  // }


  Future<void> loginUser(String? email, String? password) async {
    var qString = "${AppConfig.baseUrl + AppConfig.loginApi}";

    final Map<String, dynamic> authData = {
      'client_id': 3,
      'client_secret': 'zKu8puNCPZYTYYBRsHtw3Efz8hemktaP1LZ73aSf',
      'email': email,
      'password': password,
    };
    setBusy(true);
    try {
      var extractedData = await _authService.loginUser(qString, authData);
      setBusy(false);
      extractedData.fold(
        (failure){
          setErrorMessage(failure.message);
        },
        (success) {
          Map<String,dynamic> userData = success['data'];

          if(userData != null) {
            final loggedInUser = json.encode({
              'user_token': userData['data']['access_token'],
              'user_id': email,
              'expiryDate': DateTime.now().add(Duration(seconds: userData['data']['expires_in'])).toIso8601String()
            });

            _appPreference.setLoggedInUser(loggedInUser);

            User user = User(
                id: email,
                userToken: userData['data']['access_token'],
                expiryDate: DateTime.now().add(Duration(seconds: userData['data']['expires_in']))
            );

            setCurrentUser(user);
            // print('user :$userData');
            _navigationService.forceNavigateTo(ProductListRoute);
            notifyListeners();

          }
        }
      );
    } catch (error) {
      throw error;
    }

  }



}