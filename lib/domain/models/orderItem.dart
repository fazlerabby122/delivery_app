
import 'package:flutter/material.dart';

class OrderItem {
  int id;
  String invoiceAmount;
  // String subtotal;
  // String deliveryCharge;
  // String vat;
  // String totalBeforeVat;
  // String discount;
  // String totalDue;
  // String roundingAmount;
  // DateTime invoiceDate;
  // DateTime deliveryDate;
  // DateTime dueDate;
  DateTime createdAt;
  // String status;
  // String invoiceType;
  String customerName;
  // String customerAddress;
  String customerMobileNo;
  // String createdUserId;
  // String createdUserName;
  // String updatedUserId;
  // String updatedUserName;
  String comment;


  OrderItem({
    required this.id,
    required this.invoiceAmount,
    // required this.subtotal,
    // required this.deliveryCharge,
    // required this.vat,
    // required this.totalBeforeVat,
    // required this.discount,
    // required this.totalDue,
    // required this.roundingAmount,
    // required this.invoiceDate,
    // required this.deliveryDate,
    // required this.dueDate,
    required this.createdAt,
    // required this.status,
    // required this.invoiceType,
    required this.customerName,
    // required this.customerAddress,
    required this.customerMobileNo,
    // required this.createdUserId,
    // required this.createdUserName,
    // required this.updatedUserId,
    // required this.updatedUserName,
    required this.comment,

  });


  factory OrderItem.fromJson(Map<String, dynamic> data){
    return OrderItem(
      id : data['id'],
      invoiceAmount : data['invoice_amount'] != null ? data['invoice_amount']: '',
      // subtotal : data['sub_total'],
      // deliveryCharge : data['invoice_amount'],
      // vat : data['total_vat'],
      // totalBeforeVat : data['total_before_vat'],
      // discount : data['total_discount'],
      // totalDue : data['total_due'],
      // roundingAmount : data['rounded_amount'] != null ? data['rounded_amount'] : "0.00",
      // invoiceDate : DateTime.parse(data['invoice_date']),
      // dueDate : DateTime.parse(data['due_date']),
      createdAt : DateTime.parse(data['created_at'].toString()),
      customerName : data['customer_name'] != null ? data['customer_name']:'N/A',
      customerMobileNo : data['customer_mobile'] != null ?  data['customer_mobile'] : 'N/A',
      // status : data['status'].toString(),
      // invoiceType: data['sales_source'].toString(),
      // createdUserId: data['created_by'].toString(),
      // createdUserName: data['created_by_user'],
      // updatedUserId: data['updated_by'].toString(),
      // updatedUserName: data['user_name'],
      comment: data['comment'] != null ? data['comment'] : "",

    );
  }



  // factory OrderItem.detailFromJson(Map<String, dynamic> data){
  //   return OrderItem(
  //     id : data ['id'],
  //     invoiceAmount : data['invoice_amount'],
  //     subtotal : data['sub_total'],
  //     deliveryCharge : data['invoice_amount'],
  //     vat : data['total_vat'],
  //     totalBeforeVat : data['total_before_vat'],
  //     discount : data['total_discount'],
  //     totalDue : data['total_due'],
  //     roundingAmount : data['rounded_amount'] != null ? data['rounded_amount'] : "0.00",
  //     invoiceDate : DateTime.parse(data['invoice_date']),
  //     // deliveryDate: data['delivery_date'] != null ?DateTime.parse(data['delivery_date']) : null,
  //     dueDate : DateTime.parse(data['due_date']),
  //     createdAt : DateTime.parse(data['created_at']),
  //     customerName : data['customer_name'],
  //     customerMobileNo : data['customer_mobile'],
  //     status : data['status'].toString(),
  //     invoiceType: data['sales_source'].toString(),
  //     createdUserId: data['created_by'].toString(),
  //     createdUserName: data['created_by_user'],
  //     updatedUserId: data['updated_by'].toString(),
  //     updatedUserName: data['user_name'],
  //     comment: data['comment'],
  //   );
  // }
  //
  //
  // Map<String, dynamic> toJson() {
  //   return {
  //     'id': id,
  //     'invoice_amount': invoiceAmount,
  //     'total_due': totalDue,
  //     'invoice_date': invoiceDate,
  //     'created_at':createdAt,
  //     'customer_name':customerName,
  //     'status':status,
  //   };
  // }
}

