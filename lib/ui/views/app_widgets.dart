
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/app/navigations/route_names.dart';
import 'package:delivery_app/data/services/helperServices/navigation_service.dart';
import 'package:delivery_app/domain/viewModels/appBase_viewModel.dart';
import 'package:delivery_app/ui/widgets/app_drawer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:stacked/stacked.dart';






class AppWidgetsView extends StatelessWidget {



  AppWidgetsView({super.key});

  final CustomNavigationService _navigationService = locator<CustomNavigationService>();


  Widget _customRichText(String title, String route){
    return Center(
      child: Container(
        padding: EdgeInsets.all(10.w),
        child: RichText(
          text: TextSpan(
            children: [
              new TextSpan(
                text: title,
                style: new TextStyle(color: Colors.blue),
                recognizer: new TapGestureRecognizer()..onTap = () {
                  _navigationService.navigateTo(route);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<AppBaseViewModel>.reactive(
      viewModelBuilder: () => AppBaseViewModel(),
      onViewModelReady: (model) async{
      },
      builder: (context, model, child) =>
          Scaffold(
            backgroundColor: theme.colorScheme.background,
            appBar: AppBar(
              title: Text('Widgets example',style:theme.textTheme.titleLarge),
            ),
            drawer: AppDrawer(),
            body:Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _customRichText('Dropdown widget', BranchViewRoute),
                  _customRichText('Dialog widget', AppDialogRoute),
                  _customRichText('Image widget', CacheImageRoute),
                  _customRichText('Date time picker', DateTimePickerRoute)

                ],
              ),
            ),
          )


    );
  }
}
