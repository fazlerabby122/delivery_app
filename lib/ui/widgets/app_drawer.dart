
import 'package:flutter/material.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/app/navigations/route_names.dart';
import 'package:delivery_app/data/services/helperServices/navigation_service.dart';
import 'package:delivery_app/domain/viewModels/auth_viewModel.dart';
import 'package:delivery_app/ui/sharedWidgets/app_drawer_item.dart';
import 'package:delivery_app/ui/views/login/login_viewModel.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:stacked/stacked.dart';

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {

  final AuthViewModel _authViewModel = locator<AuthViewModel>();
  final LoginViewModel _loginViewModel = locator<LoginViewModel>();
  CustomNavigationService _navigationService = locator<CustomNavigationService>();

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<AuthViewModel>.reactive(
        viewModelBuilder :() => AuthViewModel(),
        onViewModelReady: (model) async{
        },

        builder: (context,model,child) =>Drawer(
          child: model.isBusy?Center(child: CircularProgressIndicator(),):

          ListView(
            children: <Widget>[
              Container(
                  height: 140.0.h,
                  width: MediaQuery.of(context).size.width,
                  child: DrawerHeader(
                      margin: EdgeInsets.zero,
                      padding: EdgeInsets.zero,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                      ),
                      child: Stack(children: <Widget>[
                        Positioned(
                            top: 20.0.h,
                            left: 30.0.w,
                            child: Column(
                              children: <Widget>[
                                CircleAvatar(
                                  radius: 30.0.r,
                                  backgroundImage: AssetImage('assets/profile.png'),
                                ),
                                SizedBox(height: 5.0.h),
                                model.loggedInUser!.id != null
                                    ? Text(model.loggedInUser!.id!,style:theme.textTheme.headlineSmall)
                                    : Text('Guest User',style:theme.textTheme.headlineSmall),
                              ],
                            )),
                        model.loggedInUser!.userToken == null
                            ? Positioned(
                            top: 20.0.h,
                            right: 30.0.w,
                            child: Column(
                              children: <Widget>[
                                IconButton(
                                  icon: Icon(
                                    Icons.exit_to_app,
                                    color: Colors.white,
                                    size: 25.0.r,
                                  ),
                                  onPressed: () {
                                  },
                                ),
                                Text(
                                  'Login',
                                  style:Theme.of(context).textTheme.headline1,
                                ),
                              ],
                            ))
                            : SizedBox(width: 0.0, height: 0.0,),
                      ]))),
              AppDrawerItem(
                title: 'Product list',
                titleStyle: theme.textTheme.bodyLarge,
                icon: Icon(Icons.local_grocery_store),
                onTap: (){
                  _navigationService.navigateTo(QRViewRoute);
                },
              ),
              AppDrawerItem(
                title: 'Product list',
                titleStyle: theme.textTheme.bodyLarge,
                icon: Icon(Icons.local_grocery_store),
                onTap: (){
                  _navigationService.navigateTo(ProductListRoute);
                },
              ),
              Divider(thickness: 2.w,),
              AppDrawerItem(
                title: 'Order list',
                titleStyle: theme.textTheme.bodyLarge,
                icon: Icon(Icons.inventory),
                onTap: (){
                  _navigationService.navigateTo(OrderListRoute);
                },
              ),
              Divider(thickness: 2.w,),
              AppDrawerItem(
                title: 'App Settings',
                titleStyle: theme.textTheme.bodyLarge,
                icon: Icon(Icons.settings),
                onTap: (){
                  _navigationService.navigateTo(SettingsViewRoute);
                },
              ),
              Divider(thickness: 2.w,),
              AppDrawerItem(
                title: 'Logout',
                titleStyle: theme.textTheme.bodyLarge,
                icon: Icon(Icons.power_settings_new),
                onTap: (){
                  _authViewModel.logout();
                },
              ),
              Divider(thickness: 2.w,),
            ],
          ),
        ));
  }
}


//crm-supplier-list
//accounts-bill-list