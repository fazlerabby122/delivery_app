import 'package:flutter/material.dart';
import 'package:delivery_app/app/app_local.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/app/navigations/routers.dart';
import 'package:delivery_app/app/styles/theme_setup.dart';
import 'package:delivery_app/data/services/helperServices/dialog_service.dart';
import 'package:delivery_app/data/services/helperServices/navigation_service.dart';
import 'package:delivery_app/domain/services/network_status_service.dart';
import 'package:delivery_app/domain/viewModels/appBase_viewModel.dart';
import 'package:delivery_app/ui/views/startup_view.dart';
import 'package:delivery_app/ui/widgets/common_dialog_widget.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:stacked_themes/stacked_themes.dart';
import 'package:provider/provider.dart';



void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  await ThemeManager.initialise();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});





  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {
  final AppBaseViewModel _baseViewModel = locator<AppBaseViewModel>();

  // final FlutterLocalization _localization = FlutterLocalization.instance;

  @override
  void initState() {
    _baseViewModel.localization.init(
      mapLocales: [
        const MapLocale('en', AppLocale.EN),
        const MapLocale('bn', AppLocale.BN),
        const MapLocale('ar', AppLocale.AR),
      ],
      initLanguageCode: 'en',
    );
    _baseViewModel.localization.onTranslatedLanguage = _onTranslatedLanguage;
    super.initState();
  }

  void _onTranslatedLanguage(Locale? locale) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context , child) {
        return ThemeBuilder(
          defaultThemeMode: ThemeMode.system,

          statusBarColorBuilder: (theme) => theme?.colorScheme.primary,
          navigationBarColorBuilder: (theme) => theme?.colorScheme.primary,
          themes: getThemes(),
          builder: (context, regularTheme, darkTheme, themeMode) =>
              StreamProvider<NetworkStatus>(
                  initialData: NetworkStatus.Online,
                  create: (context) => NetworkStatusService().networkStatusController.stream,
                  child: MaterialApp(
                    // builder: (context, child) => Navigator(
                    //   key: locator<CustomDialogService>().dialogNavigationKey,
                    //   onGenerateRoute: (settings) => MaterialPageRoute(
                    //       builder: (context) => DialogWidget(child: child,context: context,)),
                    // ),
                    debugShowCheckedModeBanner: false,
                    navigatorKey: locator<CustomNavigationService>().navigationKey,
                    onGenerateRoute: generateRoute,

                    supportedLocales: _baseViewModel.localization.supportedLocales,
                    localizationsDelegates: _baseViewModel.localization.localizationsDelegates,

                    title: 'Flutter App',
                    theme: regularTheme,
                    darkTheme: darkTheme,
                    themeMode: themeMode,

                    home: StartUpView(),
                  ),
              )
        );
      },
    );
  }
}






