
import 'dart:convert';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/data/dataHelper/app_prefs.dart';
import 'package:delivery_app/domain/models/user_model.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:stacked/stacked.dart';


class AppBaseViewModel extends BaseViewModel {
  final AppPreference _appPreference = locator<AppPreference>();

  static User? _loggedInUser;
  User? get loggedInUser => _loggedInUser;

  void setCurrentUser(User user){
    _loggedInUser = user;
    notifyListeners();
  }

  Future<bool> tryAutoLogin() async {
    var currentUser = await _appPreference.getLoggedInUser();
    if (currentUser == null) {
      return false;
    }

    final extractedUserData = json.decode(currentUser) as Map<String, dynamic>;
    final expiryDate = DateTime.parse(extractedUserData['expiryDate'].toString());

    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }else {
      // User user = User(
      //     id: email,
      //     userToken: userData['data']['access_token'],
      //     expiryDate: DateTime.now().add(Duration(seconds: userData['data']['expires_in']))
      // );
      User user = User(
          id: extractedUserData['user_id'].toString(),
          userToken: extractedUserData['user_token'].toString(),
          expiryDate: expiryDate
      );
      setCurrentUser(user);
      notifyListeners();
      // _autoLogout();
      return true;
    }
  }

  static  FlutterLocalization _localization = FlutterLocalization.instance;
  FlutterLocalization get localization => _localization;


  static int _perPageItem = 10;
  int get perPageItem => _perPageItem;

  static int _pageCount=1;
  int get pageCount => _pageCount;
  void setPageCount(int value){
    _pageCount = value;
    notifyListeners();
  }

  void incrementPageCount(){
    _pageCount += 1;
    notifyListeners();
  }

  static int _oldPageCount = 0 ;
  int get oldPageCount => _oldPageCount;
  void setOldPageCount(int value){
    _oldPageCount = value;
    notifyListeners();
  }

  void incrementOldPageCount(){
    _oldPageCount += 1;
    notifyListeners();
  }

  static int? _lastPageNo;
  int? get lastPageNo => _lastPageNo;
  void setLastPageNo(int value){
    _lastPageNo = value;
    notifyListeners();
  }

  static bool _isLastPage = false;
  bool get isLastPage => _isLastPage;
  void setIsLastPage(bool value){
    _isLastPage = value;
    notifyListeners();
  }

  void refreshPageCount(){
    _pageCount = 1;
    _oldPageCount = 0;
    notifyListeners();
  }


  static String? _errorMessage;
  String? get errorMessage => _errorMessage;
  void setErrorMessage(String value){
    _errorMessage = value;
    notifyListeners();
  }


}