part of './../base_color_theme.dart';

class LightColors implements BaseColorTheme {
  @override
  final AppColors colors = AppColors();

  @override
  Color? backgroundColor = Colors.white;

  @override
  Color? primaryColor = Colors.grey;

  @override
  ColorScheme? colorScheme;

  @override
  Color? appBarColor=Colors.grey[300];

  @override
  Color? scaffoldBackgroundColor;

  @override
  Color? tabBarColor;

  @override
  Color? tabbarNormalColor;

  @override
  Color? tabbarSelectedColor;

  LightColors() {
    appBarColor;
    scaffoldBackgroundColor = colors.white;
    tabBarColor = colors.green;
    tabbarNormalColor = colors.darkerGrey;
    tabbarSelectedColor = colors.green;
    colorScheme = ColorScheme.light().copyWith(
      background: backgroundColor,
      primary: primaryColor,
      onPrimary: colors.darkerGrey,
      onSecondary: colors.green,
      onSurface: colors.mediumGreyBold);
    brightness = Brightness.light;
  }

  @override
  Brightness? brightness;


}
