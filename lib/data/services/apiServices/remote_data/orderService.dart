


import 'package:dartz/dartz.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/data/dataHelper/error_handler.dart';
import 'package:delivery_app/data/dataHelper/failure.dart';
import 'package:delivery_app/data/services/helperServices/dio_service.dart';

class OrderService {


  final DioService _dioFactory = locator<DioService>();


  Future<dynamic> loadOrders(var qString,var token) async {
    print(qString);

    Map<String,dynamic> responseData= Map();

    try {
      final response = await _dioFactory.getDioWithToken(qString,token);
      if(response.statusCode == ResponseCode.SUCCESS) {
        responseData.putIfAbsent('data', () => response.data);
        return Right(responseData);
      }else{
        return Left(FailureItem(
            response.statusCode ?? 404,
            response.statusMessage ?? ResponseMessage.DEFAULT)
        );
      }
    } catch (error) {
      return (Left(ErrorHandler.handle(error).failure));
    }
  }

}
