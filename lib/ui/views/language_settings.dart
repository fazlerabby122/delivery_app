
import 'package:flutter/material.dart';
import 'package:delivery_app/app/app_local.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/domain/viewModels/appBase_viewModel.dart';
import 'package:flutter_localization/flutter_localization.dart';





class LanguageSettingsView extends StatefulWidget {
  LanguageSettingsView({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<LanguageSettingsView> {

  final AppBaseViewModel _appBaseViewModel = locator<AppBaseViewModel>();


  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(title: Text(AppLocale.settingsTitle.getString(context))),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Current language is: ${_appBaseViewModel.localization.getLanguageName()}',style:theme.textTheme.titleSmall),
            const SizedBox(height: 64.0),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    child: const Text('English'),
                    onPressed: () {
                      _appBaseViewModel.localization.translate('en');
                    },
                  ),
                ),
                const SizedBox(width: 8.0),
                Expanded(
                  child: ElevatedButton(
                    child: const Text('বাংলা'),
                    onPressed: () {
                      _appBaseViewModel.localization.translate('bn');
                    },
                  ),
                ),
                const SizedBox(width: 8.0),
                Expanded(
                  child: ElevatedButton(
                    child: const Text('اَلْعَرَبِيَّةُ'),
                    onPressed: () {
                      _appBaseViewModel.localization.translate('ar', save: false);
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}









// import 'package:dinninn/ui/utils/LocaleHelper.dart';
// import 'package:dinninn/ui/utils/app_localizations.dart';
// import 'package:dinninn/ui/utils/new_app_localizations.dart';
// import 'package:dinninn/ui/utils/strings.dart';
// import 'package:dinninn/ui/views/landing_page/landing_page_view.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
//
// class PreferredLanguageSettings extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return _PreferredLanguageSettings();
//   }
// }
//
// class _PreferredLanguageSettings extends State<PreferredLanguageSettings>{
//   SpecificLocalizationDelegate _specificLocalizationDelegate;
//   int langGroupVal = 0;
//
//
//
//   @override
//   void initState() {
//     super.initState();
//     helper.onLocaleChanged = onLocaleChange;
//
//     print("InitMain 2 > Curr Locale: " + LocaleHelper.currLocale.toString());
//     _specificLocalizationDelegate = SpecificLocalizationDelegate(overriddenLocale: new Locale(Strings.dummyLocaleInit), isTemp: true);
//     print("InitMain 3 > Curr Locale: " + _specificLocalizationDelegate.overriddenLocale.languageCode);
//   }
//
//   onLocaleChange(Locale locale) {
//     helper.setLanguagePref(locale.languageCode);
//     if (!_specificLocalizationDelegate.isTemp) {
//       setState(() {
//         _specificLocalizationDelegate = new SpecificLocalizationDelegate(overriddenLocale: locale);
//       });
//     } else if (locale.languageCode != Strings.dummyLocale) {
//       setState(() {
//         _specificLocalizationDelegate = new SpecificLocalizationDelegate(overriddenLocale: locale);
//       });
//     } else {
//       setState(() {
//         _specificLocalizationDelegate = new SpecificLocalizationDelegate(overriddenLocale: locale, isTemp: true);
//       });
//     }
//   }
//
//
//   @override
//   Widget build(BuildContext context) {
//
//     return Scaffold(
//       body: Container(
//         alignment: Alignment.center,
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           mainAxisSize: MainAxisSize.min,
//           children: <Widget>[
//             Container(
//               child: Text('Select Language'),
//               padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
//               color: Colors.blueAccent,
//             ),
//             Row(
//               mainAxisSize: MainAxisSize.min,
//               children: <Widget>[
//                 Radio(
//                   value: 0,
//                   groupValue: langGroupVal,
//                   onChanged: (int value) {
//                     setState(() {
//                       langGroupVal = value;
//                     });
//                   },
//                 ),
//                 new Text(
//                   'English',
//                   style: Theme.of(context).textTheme.bodyText1,
//                 ),
//               ],
//             ),
//             Row(
//               mainAxisSize: MainAxisSize.min,
//               children: <Widget>[
//                 Radio(
//                   value: 1,
//                   groupValue: langGroupVal,
//                   onChanged: (int value) {
//                     setState(() {
//                       // langGroupVal = value;
//                       NewAppLocalizations.of(context).locale = Locale('ar','BN');
//                     });
//                   },
//                 ),
//                 new Text(
//                   'বাংলা',
//                   style: Theme.of(context).textTheme.bodyText1,
//                 ),
//               ],
//             ),
//             RaisedButton(
//               child: Text('Continue'),
//               onPressed: (){
//                 onLocaleChange(new Locale(Strings.locales.elementAt(langGroupVal)));
//                 Navigator.push(context, MaterialPageRoute(builder: (context) => LandingPageView()));
//               },
//             )
//           ],
//         ),
//       ),
//     );
//   }
//
// }