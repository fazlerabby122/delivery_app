part of './../base_color_theme.dart';


class DarkColors implements BaseColorTheme {
  @override
  final AppColors colors = AppColors();

  @override
  Color? backgroundColor = Colors.black;

  @override
  Color? primaryColor = Colors.white;

  @override
  ColorScheme? colorScheme;

  @override
  Brightness? brightness;

  @override
  Color? appBarColor = Colors.blueGrey;

  @override
  Color? scaffoldBackgroundColor;

  @override
  Color? tabBarColor;

  @override
  Color? tabbarNormalColor;

  @override
  Color? tabbarSelectedColor;


  DarkColors() {
    appBarColor;
    scaffoldBackgroundColor = colors.darkGrey;
    tabBarColor = colors.green;
    tabbarNormalColor = colors.lighterGrey;
    tabbarSelectedColor = colors.green;
    colorScheme = ColorScheme.dark().copyWith(
      background: backgroundColor,
      primary: primaryColor,
      onPrimary: Colors.red,
      onSecondary: colors.white);
    brightness = Brightness.dark;
  }
}
