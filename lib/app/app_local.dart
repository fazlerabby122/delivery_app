

mixin AppLocale {
  static const String title = 'title';
  static const String settingsTitle = 'settingsTitle';
  static const String home = 'home';
  static const String productList = 'productList';
  static const String confirmButtonText ="confirmButtonText";
  static const String loginButtonText ="loginButtonText";
  static const String noInternetConnection ="noInternetConnection";
  static const String chooseLanguage = 'chooseLanguage';
  static const String confirm = 'confirm';
  static const String cancel = 'cancel';

  static const Map<String, dynamic> EN = {
    title: 'English',
    settingsTitle: 'Localization',
    home:'Home',
    productList: 'Product list',
    confirmButtonText: 'Confirm',
    loginButtonText: 'Login',
    noInternetConnection: 'No internet conneticon',
    chooseLanguage: 'Choose language',
    confirm : 'Confirm',
    cancel : 'Cancel'

  };
  static const Map<String, dynamic> BN = {
    title: 'বাংলা',
    settingsTitle: 'স্থানীয়করণ',
    home:'বাড়ি',
    productList:'পণ্য তালিকা',
    confirmButtonText: 'নিশ্চিত করুন',
    loginButtonText: 'প্রবেশ করুন',
    noInternetConnection: 'ইন্টারনেট সংযোগ নেই',
    chooseLanguage: 'ভাষা নির্বাচন করুন',
    confirm : 'নিশ্চিত করুন',
    cancel : 'বাতিল'
  };
  static const Map<String, dynamic> AR = {
    title: 'اَلْعَرَبِيَّةُ',
    settingsTitle: 'الموقع',
    home:'بيت',
    productList: 'المنتجات',
    confirmButtonText: 'يتأكد',
    loginButtonText: 'تسجيل الدخول',
    noInternetConnection: 'لا يوجد اتصال بالإنترنت',
    chooseLanguage: 'اختر اللغة',
    confirm: 'يتأكد',
    cancel: 'يلغي'
  };
}