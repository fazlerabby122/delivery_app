
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

class AppTimePicker extends StatelessWidget{

  AppTimePicker({
    required this.showTitleActions,
    this.isEditable = false,
    this.value,
    this.valueStyle,
    this.hintText = '',
    this.hintStyle,
    this.headerColor,
    required this.backgroundColor,
    required this.itemStyle,
    required this.doneStyle,
    required this.cancelStyle,
    required this.timeFormat,
    this.controller,
    required this.onChanged,
    required this.onConfirm,
    Key? key,
  }) : super(key: key);

  final bool showTitleActions;
  bool? isEditable;
  final String? value;
  final TextStyle? valueStyle;
  final String hintText;
  final TextStyle? hintStyle;
  final Color? headerColor;
  final Color backgroundColor;
  final TextStyle itemStyle;
  final TextStyle doneStyle;
  final TextStyle cancelStyle;
  final DateFormat timeFormat;
  final Function(DateTime) onChanged;
  final Function(DateTime)  onConfirm;
  final TextEditingController? controller;


  String convert12(String str) {
    print('str : ' + str);
    String finalTime='';

      int h1 = int.parse(str.substring(0, 1)) - 0;
    // if(h1>12){
      int h2 = int.parse(str.substring(1, 2));
      int hh = h1 * 10 + h2;

      String Meridien;
      if (hh < 12) {
        Meridien = " AM";
      } else
        Meridien = " PM";
      hh %= 12;
      if (hh == 0 && Meridien == ' PM') {
        finalTime = '12' + str.substring(2);
      } else {
        finalTime = hh.toString() + str.substring(2);
      }
      finalTime = finalTime + Meridien;
    // }
    return finalTime;
  }
  // + convert12(DateFormat('hh:mm').format( DateTime.now().toLocal()))
  @override
  Widget build(BuildContext context) {
    return
      SizedBox(
          width:250,
          child:Row(
            children: [
              SizedBox(width: 150.w,
                child: TextField(
                  textAlign: TextAlign.center,
                  enabled: isEditable,
                  controller:TextEditingController(text: value != 'null' ? timeFormat.format(DateTime.parse(value!)):'',),
                  // controller:TextEditingController(text: value != 'null' ? convert12(timeFormat.format(DateTime.parse(value!))):'',),
                  style: valueStyle,
                  decoration: InputDecoration(
                      hintText: hintText,
                      hintStyle:hintStyle,
                  ),
                ),),
              SizedBox(width: 10.w),
              IconButton(
                icon: Icon(Icons.timer_sharp,size: 30.w,),
                onPressed: () {
                  DatePicker.showTimePicker(context,
                      showTitleActions: showTitleActions,
                      theme: DatePickerTheme(
                        headerColor: headerColor,
                        backgroundColor: backgroundColor,
                        itemStyle: itemStyle,
                        doneStyle: doneStyle,
                        cancelStyle: cancelStyle,
                      ),
                      onChanged: (date) => onChanged(date),
                      onConfirm: (date) => onConfirm(date),
                      currentTime: DateTime.now(),
                      locale: LocaleType.en);
                },
              )
            ],
          )
      );
  }

}