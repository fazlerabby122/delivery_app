
import 'package:delivery_app/app/app_config.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/app/navigations/route_names.dart';
import 'package:delivery_app/data/dataHelper/app_prefs.dart';
import 'package:delivery_app/data/services/apiServices/remote_data/authService.dart';
import 'package:delivery_app/data/services/helperServices/navigation_service.dart';
import 'package:delivery_app/domain/viewModels/appBase_viewModel.dart';


class AuthViewModel extends AppBaseViewModel {

  final AppPreference _appPreference = locator<AppPreference>();
  final AuthService _authService = locator<AuthService>();
  final CustomNavigationService _navigationService = locator<CustomNavigationService>();



  void logout() async {

    var qString = "${AppConfig.baseUrl}access-control/user/logout";

    try {
      setBusy(true);
      var data = await _authService.logoutUser(qString,loggedInUser!.userToken,'');
      setBusy(false);
      data.fold(
        (failure){
          setErrorMessage(failure.message);
          },
        (success){
          var data = success['data']['msg'];
          _navigationService.forceNavigateTo(LoginViewRoute);
        }
      );
      notifyListeners();
    }
    catch (error) {
      throw (error);
    }

    _appPreference.logOutUse();
    notifyListeners();
  }

}