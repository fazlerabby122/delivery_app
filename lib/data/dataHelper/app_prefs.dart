
import 'package:shared_preferences/shared_preferences.dart';

const String PREFS_KEY_LANG = "PREFS_KEY_LANG";
const String PREFS_KEY_ONBOARDING_SCREEN = "PREFS_KEY_ONBOARDING_SCREEN";
const String PREFS_KEY_IS_USER_LOGGED_IN = "PREFS_KEY_IS_USER_LOGGED_IN";
const String LOGEDIN_USER = "userData";

class AppPreference {
  SharedPreferences _sharedPreferences;

  AppPreference(this._sharedPreferences);
  // final sharedPrefs = await SharedPreferences.getInstance();



  Future<void> setLoggedInUser(var userData) async{
    await _sharedPreferences.setString('userData', userData);
  }

  Future<dynamic> getLoggedInUser() async{
    if(_sharedPreferences.containsKey('userData')) {
      final currentUserData = _sharedPreferences.getString('userData',);
      if (currentUserData!.isEmpty) {
        return null;
      }
      return currentUserData;
    }
  }

  Future<void> logOutUse() async{
    if(_sharedPreferences.containsKey('userData')) {
      _sharedPreferences.remove('userData');
    }
  }



  Future<void> setIsUserLoggedIn() async {
    _sharedPreferences.setBool(PREFS_KEY_IS_USER_LOGGED_IN, true);
  }

  Future<bool> isUserLoggedIn() async {
    return _sharedPreferences.getBool(PREFS_KEY_IS_USER_LOGGED_IN) ?? false;
  }





// Future<String> getAppLanguage() async {
//   String? language = _sharedPreferences.getString(PREFS_KEY_LANG);
//
//   if (language != null && language.isNotEmpty) {
//     return language;
//   } else {
//     return LanguageType.ENGLISH.getValue();
//   }
// }

// Future<void> setOnBoardingScreenViewed() async {
//   _sharedPreferences.setBool(PREFS_KEY_ONBOARDING_SCREEN, true);
// }
//
// Future<bool> isOnBoardingScreenViewed() async {
//   return _sharedPreferences.getBool(PREFS_KEY_ONBOARDING_SCREEN) ?? false;
// }
}
