
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class AppImageWidget extends StatelessWidget {
  final double width;
  final double height;
  final bool isRoundedCorner;
  final String imageUrl;
  final String placeholder;
  final String errorImage;

  
  const AppImageWidget({super.key,
    required this.width,
    required this.height,
    required this.isRoundedCorner,
    required this.imageUrl,
    required this.placeholder,
    required this.errorImage
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: Center(
        child: CachedNetworkImage(
          imageUrl: imageUrl ,
          placeholder: (context,url)  {return Image.asset(placeholder);},
          errorWidget: (context,url,error)  {return Center(child:Image.asset(errorImage));},
        ),
      ),

    );
  }
}