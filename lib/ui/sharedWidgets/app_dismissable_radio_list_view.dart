// //
// //
// // import 'package:flutter/cupertino.dart';
// // import 'package:flutter/material.dart';
// // import 'package:delivery_app/ui/sharedWidgets/app_list_item.dart';
// // import 'package:delivery_app/ui/views/productList/product_viewModel.dart';
// // import 'package:flutter_screenutil/flutter_screenutil.dart';
// // import 'package:pull_to_refresh/pull_to_refresh.dart';
// //
// // class AppDismissableRadioListView<T> extends StatelessWidget {
// //   T model;
// //   final List<BaseListItem> items;
// //   final Function(T) loadMore;
// //   final RefreshController refreshController;
// //   final ScrollController scrollController;
// //
// //   AppDismissableRadioListView({super.key, required this.model,required this.items,required this.loadMore,required this.refreshController,required this.scrollController});
// //
// //   @override
// //   Widget build(BuildContext context) {
// //     return SmartRefresher(
// //         enablePullUp: true,
// //         enablePullDown: false,
// //         onRefresh: () async{
// //           print('refreshing');
// //           await loadMore(model);
// //           refreshController.refreshCompleted();
// //         },
// //         onLoading: () async{
// //           print('loading');
// //           await loadMore(model);
// //           refreshController.loadComplete();
// //         },
// //         controller: refreshController,
// //
// //
// //
// //
// //
// //         footer: CustomFooter(
// //           builder: (BuildContext context,LoadStatus? mode){
// //             Widget body;
// //             if(mode==LoadStatus.idle){
// //               body =  Text("No more product",style: TextStyle(color: Colors.black),);
// //             }
// //             else if(mode==LoadStatus.loading){
// //               body =  CupertinoActivityIndicator();
// //             }
// //             else if(mode == LoadStatus.failed){
// //               body = Text("Load Failed!Click retry!");
// //             }
// //             else if(mode == LoadStatus.canLoading){
// //               body = Text("release to load more");
// //             }
// //             else{
// //               body = Text("No more Product",style: TextStyle(color: Colors.black),);
// //             }
// //             return Container(
// //               height: 40.0.h,
// //               child: Center(child:body),
// //             );
// //           },
// //         ),
// //         child:
// //         Dismissible(
// //
// //           key: UniqueKey(),
// //           direction: DismissDirection.endToStart,
// //           background: Container(
// //             color: Theme.of(context).errorColor,
// //             child: Icon(
// //               Icons.delete,
// //               color: Colors.white,
// //               size: 40,
// //             ),
// //             alignment: Alignment.centerRight,
// //             padding: EdgeInsets.only(right: 20),
// //             // margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
// //           ),
// //           child: Container(
// //             child:
// //             // ListView.builder(
// //             //   controller: scrollController,
// //             //   itemCount: items.length,
// //             //   itemBuilder: (context, index) {
// //             //     final item = items[index];
// //             //     return ListTile(
// //             //       leading: item.buildLeading(context),
// //             //       title: item.buildTitle(context),
// //             //       subtitle: item.buildSubtitle(context),
// //             //       trailing: item.buildTrail(context),
// //             //     );
// //             //   },
// //             // ),
// //
// //
// //             RadioListTile(
// //
// //               value:data,
// //               groupValue: _shippingAddress.selectedAddress,
// //               title: Text(data.shippingAddress,style: Theme.of(context).textTheme.bodyText1,),
// //               secondary: InkWell(
// //                 child: Icon(Icons.edit,size: 22.0,color: Colors.red,),
// //                 onTap: () async {
// //                   await showDialog(
// //                       context: context,
// //                       barrierDismissible: false,
// //                       builder: (context) => UpdateShippingAddressDialog(
// //                         addressItem: data,
// //                       ));
// //                   await model.fetchShippingAddressList();
// //                 },
// //               ),
// //               onChanged: (dynamic currentAddress) {
// //                 print("New address ${currentAddress.id}");
// //
// //               },
// //               selected:_shippingAddress.selectedAddress!=null ? data.id == _shippingAddress.selectedAddress!.id :false,
// //               activeColor: Theme.of(context).primaryColor,
// //               selectedTileColor: Colors.grey[300],
// //             ),
// //
// //           ),
// //           confirmDismiss: (direction) async{
// //             // _dialogService.showConfirmationDialog(title: 'Remove confirmation', description: 'Do you want to remove this address?',cancelButtonTitle: 'No',confirmationButtonTitle: 'Yes');
// //             return showDialog(
// //                 context: context,
// //                 barrierDismissible: false,
// //                 builder: (context) => AlertDialog(
// //                   title: Text('Remove confirmation',textAlign: TextAlign.center,style: Theme.of(context).textTheme.headline1,),
// //                   content: Text('Do you want to remove this address?',textAlign: TextAlign.center,),
// //                   actions: <Widget>[
// //                     TextButton(child: Text('No'), onPressed: (){Navigator.of(context).pop(false);},),
// //                     TextButton(child: Text('Yes'), onPressed: (){Navigator.of(context).pop(true);},),
// //                   ],
// //                 )
// //             );
// //           },
// //           onDismissed: (direction) async {
// //
// //           },
// //         ),
// //       );
// //   }
// // }
// //
//
//
//
//
// import 'package:flutter/material.dart';
// import 'package:delivery_app/ui/sharedWidgets/app_list_view.dart';
// import 'package:delivery_app/ui/sharedWidgets/app_radio_list_view.dart';
// import 'package:delivery_app/ui/views/productList/product_viewModel.dart';
// import 'package:delivery_app/ui/widgets/app_drawer.dart';
// import 'package:pull_to_refresh/pull_to_refresh.dart';
// import 'package:stacked/stacked.dart';
//
//
//
//
// class RadioListTileExample extends StatefulWidget{
//   @override
//   RadioListTileExampleState createState() => RadioListTileExampleState();
//
// }
//
// class RadioListTileExampleState extends State<RadioListTileExample> {
//
//   final RefreshController refreshController = RefreshController();
//   static ScrollController scrollController = ScrollController();
//
//
//   _loadMoreProducts(ProductViewModel model) async{
//     await model.fetchAndSetMoreProducts();
//   }
//
//   onChangeCall(ProductViewModel model){
//     print(model.products.length.toString());
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     var theme = Theme.of(context);
//     return ViewModelBuilder<ProductViewModel>.reactive(
//         viewModelBuilder: () => ProductViewModel(),
//         onViewModelReady: (model) async{
//           await model.fetchAndSetProducts();
//         },
//         builder: (context, model, child) => Scaffold(
//           backgroundColor: theme.colorScheme.background,
//           appBar: AppBar(title: const Text('Product list'),),
//           drawer: AppDrawer(),
//           body:
//
//           model.isBusy ? Center(child: CircularProgressIndicator()) :
//           Container(
//             child: Column(
//               children: <Widget>[
//                 Expanded(
//                     child:
//                     Container(child: model.products != null && model.products.length > 0 ?
//
//                     AppRadioListView<ProductViewModel>(
//                       refreshController: refreshController,
//                       scrollController: scrollController,
//                       loadMore: _loadMoreProducts,
//                       model: model,
//                       items: List<BaseRadioListItem>.generate(
//                         model.products.length, (i) =>
//                           AppRadioListItem(
//                             title: 'name :  ${model.products[i].title}',
//                             titleStyle:theme.textTheme.displaySmall,
//                             subTitle: 'slug :  ${model.products[i].quantityLeft}',
//                             subTitleStyle:theme.textTheme.bodySmall,
//                             // onChangeCall: onChangeCall,
//                           ),),
//                     ):Center(child: Text('no product found')),
//
//                       //// ListViewWidget(model:model,products:model.products,loadMore: _loadMoreProducts,): Center(child: Text('No item found',style: Theme.of(context).textTheme.subtitle1),)
//                     )
//                 ),
//
//               ],
//             ),
//           ),
//         )
//     );
//   }
// }
//
//
//
//
//
//
//
// //
// // class RadioListTileExample extends StatefulWidget{
// //   @override
// //   RadioListTileExampleState createState() => RadioListTileExampleState();
// //
// // }
//
//
// // class RadioListTileExampleState extends State<RadioListTileExample> {
// //   @override
// //   Widget build(BuildContext context) {
// //     return MaterialApp(
// //       home: Scaffold(
// //         body: RadioListBuilder(
// //           // num: 20,
// //         ),
// //       ),
// //     );
// //   }
// // }
// //
// // class RadioListBuilder extends ViewModelWidget<ProductViewModel> {
// //
// //   int value = 0;
// //   final RefreshController refreshController = RefreshController();
// //   static ScrollController scrollController = ScrollController();
// //
// //   _loadMoreProducts(ProductViewModel model) async{
// //     await model.fetchAndSetMoreProducts();
// //   }
// //
// //
// //   @override
// //   Widget build(BuildContext context, ProductViewModel model) {
// //     var theme = Theme.of(context);
// //     return AppRadioListView<ProductViewModel>(
// //       refreshController: refreshController,
// //       scrollController: scrollController,
// //       loadMore: _loadMoreProducts,
// //       model: model,
// //       items: List<BaseRadioListItem>.generate(
// //         model.products.length, (i) =>
// //           AppRadioListItem(
// //             title: 'name :  ${model.products[i].title}',
// //             titleStyle:theme.textTheme.displaySmall,
// //             subTitle: 'slug :  ${model.products[i].quantityLeft}',
// //             subTitleStyle:theme.textTheme.bodySmall,
// //           ),),
// //     );
// //   }
// // }
//
//
//
// class AppRadioListItem<T> implements BaseRadioListItem {
//   final String title;
//   final TextStyle? titleStyle;
//   final String subTitle;
//   final TextStyle? subTitleStyle;
//   final Function(T?)? onChangeCall;
//
//   AppRadioListItem({required this.title,required this.titleStyle, required this.subTitle, required this.subTitleStyle,required this.onChangeCall});
//
//   @override
//   int? groupValue;
//
//   @override
//   void Function(dynamic?)? onChange;
//
//   @override
//   int? value;
//
//   @override
//   Widget buildTitle(BuildContext context) => Text(title,style: titleStyle,);
//
//   @override
//   Widget buildSubtitle(BuildContext context) => Text(subTitle,style: subTitleStyle,);
//
//
//
// }
//
//
// abstract class BaseRadioListItem {
//   int? value;
//   int? groupValue;
//   void Function(dynamic?)? onChange;
//   Widget buildTitle(BuildContext context);
//   Widget buildSubtitle(BuildContext context);
// }