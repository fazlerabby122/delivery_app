import 'package:flutter/material.dart';
import 'package:delivery_app/app/styles/themes/colors/base_color_theme.dart';
import 'package:delivery_app/app/styles/themes/text/themeTexts/dark_text.dart';
import 'package:delivery_app/app/styles/themes/text/themeTexts/light_text.dart';
import 'text/base_text_theme.dart';

abstract class BaseTheme {
  BaseTextTheme get textTheme;
  BaseColorTheme get colors;
}

abstract class ThemeManager {
  static ThemeData craeteTheme(BaseTheme theme) => ThemeData(
    fontFamily: theme.textTheme.fontFamily,
    textTheme: theme.textTheme.data,
    cardColor: theme.colors.colorScheme?.onSecondary,
    // colorScheme: ColorScheme.fromSwatch()
    //     .copyWith(secondary: theme.colors.colors.green)
    //     .copyWith(background: theme.colors.colors.lightGrey)
    //     .copyWith(error: Colors.red),
    colorScheme: theme.colors.colorScheme,
    tabBarTheme: TabBarTheme(
      indicator: BoxDecoration(),
      labelColor: theme.colors.tabbarSelectedColor,
      unselectedLabelColor: theme.colors.tabbarNormalColor,
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
        foregroundColor: theme.colors.colors.white,
        backgroundColor: theme.colors.colors.mediumGreyBold),
    appBarTheme: AppBarTheme(backgroundColor: theme.colors.appBarColor,titleTextStyle: theme.textTheme.titleLarge),
    scaffoldBackgroundColor: theme.colors.scaffoldBackgroundColor,
    bottomAppBarTheme: BottomAppBarTheme(color: Colors.green),
  );
}

class AppThemeDark extends BaseTheme {
  @override
  late final BaseTextTheme textTheme;
  AppThemeDark() {
    textTheme = TextThemeDark(colors.colors.white);
  }

  @override
  BaseColorTheme get colors => DarkColors();
}

class AppThemeLight extends BaseTheme {
  @override
  late final BaseTextTheme textTheme;

  AppThemeLight() {
    textTheme = TextThemeLight(colors.colors.darkerGrey);
  }

  @override
  BaseColorTheme get colors => LightColors();
}
