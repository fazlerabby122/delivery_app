
import 'package:flutter/material.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/app/navigations/route_names.dart';
import 'package:delivery_app/data/services/helperServices/navigation_service.dart';
import 'package:delivery_app/domain/viewModels/appBase_viewModel.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:stacked/stacked.dart';






class StartUpView extends StatelessWidget {



  StartUpView({super.key});

  final CustomNavigationService _navigationService = locator<CustomNavigationService>();


  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AppBaseViewModel>.reactive(
      viewModelBuilder: () => AppBaseViewModel(),
      onViewModelReady: (model) async{
        // await model.tryAutoLogin();
        Future.delayed(const Duration(seconds: 1),(){
          // if(model.loggedInUser != null){
          //   _navigationService.navigateTo();
          // }else{
          //   _navigationService.navigateTo();
          // }
          _navigationService.navigateTo(BarcodeViewRoute);

        });


      },
      builder: (context, model, child) => Material(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 2,
                child:
                Container(
                  padding: EdgeInsets.only(top: 100.w),
                  child: Image(
                    width: 200.w,
                    image: const AssetImage(
                      'assets/images/logo.png'
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.only(top: 100.w),
                  child: const Text(
                    'version 1.0',
                    style: TextStyle(color: Colors.black38),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
