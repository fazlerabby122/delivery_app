import 'package:flutter/material.dart';


class AppDrawerItem extends StatelessWidget {


  final String title;
  final TextStyle? titleStyle;
  final void Function(String)? onChanged;
  final VoidCallback? onTap;
  final Icon? icon;

  const AppDrawerItem({
    required this.title,
    required this.titleStyle,
    this.onChanged,
    required this.onTap,
    required this.icon,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: icon,
      title: Text(title,style:titleStyle),
      onTap: onTap,
    );
  }
}
