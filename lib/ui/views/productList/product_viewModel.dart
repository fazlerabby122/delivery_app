
import 'dart:convert';
import 'dart:io';

import 'package:delivery_app/app/app_config.dart';
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/data/services/apiServices/remote_data/productService.dart';
import 'package:delivery_app/domain/models/product_item.dart';
import 'package:delivery_app/domain/viewModels/appBase_viewModel.dart';
import 'package:flutter/services.dart';

class ProductViewModel extends AppBaseViewModel{

  final ProductService _productService = locator<ProductService>();

  static List<Product> _products = [];
  List<Product> get products => _products;

  static Product? _product;
  Product? get product => _product;


  var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiYWQwM2E5NjUyOTM2OWNjZDFkOWE4MTEyODI3ZmRiMGExNDZmZTVkM2I0OTA2ZTYxOTE2OWUxY2U2OWJkMzk5YmJkOTUxZWE2MDhmOGYxNmUiLCJpYXQiOjE2Nzg5NTUyMTAsIm5iZiI6MTY3ODk1NTIxMCwiZXhwIjoxNzEwNTc3NjEwLCJzdWIiOiIxMSIsInNjb3BlcyI6WyIqIl19.OhPMqs_QC0h7tzsWCYsznI3RcqjFnxU9-whWia9nDltjJ65vTkH0CmMuj81sgrpwCjGCKBr3qrGH976ivN-BJGEpGkRn_n1nvjDz0Tf8Gd2lnNda-aXDQojA5-EhXcYsyGtBAZVYxJ1hQIWv3nhysYMOK4-Omf_D_bIrIJy0-6AgGpMCypsekk844H-dlVmNouk1_ypa40TNGVFQIG8Zfrs2Q29t98XT8xMzQWjCjXhBwNyRyzBoo-G1_hvxjJE_WnVAOLN-uwcvLLJUvg5AKAbBcnzpKJMQ55-jXHpXSjOJcwiuumB2iCNVZNG_pGG63BR6McVN0qm4t5mqAHXl61UBlFgc8_6hzW5K-8of8-d-8VN-6xg1zLS5Cx5oRWaAprq3LoKhYIjZm1oFaQbS0nGIVr8tEfmHSKMX9yC46LUouZph8R50ZSQjT6KkgFeNCSdj9sVRfgHJhBhxSIJO8hChpptCcJakSITE0TPPyjmjAUsUFsDtGfrPXT4F9b4kBmpK-7Oed-Aj0kugPJLmOqQ1IdSnhUIFM49i_JYzfcCPbbyoGlJUo5q_h2-hVC858qKF38SOgbiQfMuU_WQq3q_hYCiRzp32ck1s-iMbjAWXVLRAaHw4l41e34jpGipj2NfE5_LlzFcIqfL2Gkk4Vi2WktLOxa9OnH15GWmxIuA";
  processData(var extractedData){
    final List<Product> loadedProducts = [];
    var allProduct = extractedData['data']['data'];
    setLastPageNo(extractedData['data']['last_page']);
    setOldPageCount(0);

    for (int i = 0; i < allProduct.length; i++) {
      final Product product = Product.fromJson(allProduct[i],'');
      loadedProducts.add(product);
    }
    _products = loadedProducts;
    notifyListeners();
  }

  Future<void> fetchProduct(var barcode) async {
    refreshPageCount();
    var qString = "${AppConfig.productDetailApi}?barcode=$barcode&resell=true&product_price_category_id=1";
    try {
      setBusy(true);
      var data = await _productService.loadProduct(qString,token);
      setBusy(false);
      data.fold(
          (failure){
            setErrorMessage(failure.message);
          },(data){
        var productData = data['data']['data'];
        print('product resutl  :' + productData.toString());
        _product = Product.fromJson(productData,'');
        print('processed product : ' + _product.toString());
      }
      );

      notifyListeners();
    }
    catch (error) {
      rethrow;
    }
  }


  // Future<void> fetchAndSetProducts() async {
  //   refreshPageCount();
  //   var qString = "${AppConfig.baseUrl + AppConfig.productFetchApi}?page_size=$perPageItem&page=$pageCount&product_price_category_id=1&category_id=0&branch_id=1&sort_order=desc";
  //   try {
  //     setBusy(true);
  //     var data = await _productService.loadProducts(qString,loggedInUser!.userToken);
  //     setBusy(false);
  //     data.fold(
  //         (failure){
  //           setErrorMessage(failure.message);
  //         },(data){
  //       var productData = data['data'];
  //       processData(productData);
  //     }
  //     );
  //
  //     notifyListeners();
  //   }
  //   catch (error) {
  //     rethrow;
  //   }
  // }
  //
  //
  // Future<void> fetchAndSetMoreProducts() async {
  //   var qString = "${AppConfig.baseUrl + AppConfig.productFetchApi}?page_size=$perPageItem&page=$pageCount&product_price_category_id=1&category_id=0&branch_id=1&sort_order=desc";
  //
  //   if(pageCount - oldPageCount == 1  && !isLastPage) {
  //     print('more item');
  //     incrementPageCount();
  //     var extractedData = await _productService.loadProducts(qString,loggedInUser!.userToken);
  //
  //     extractedData.fold(
  //           (failure){
  //             setErrorMessage(failure.message);
  //           },
  //           (data){
  //       final List<Product> loadedProducts = [];
  //       List<Product> newLoadedProducts = [];
  //       var productData = data['data'];
  //
  //       var allProduct = productData['data']['data'];
  //
  //       setLastPageNo(productData['data']['last_page']);
  //       incrementOldPageCount();;
  //       for (int i = 0; i < allProduct.length; i++) {
  //         final Product product = Product.fromJson(allProduct[i],'');
  //         loadedProducts.add(product);
  //       }
  //       newLoadedProducts = loadedProducts;
  //       _products.addAll(newLoadedProducts);
  //       print('products count : ' + _products.length.toString());
  //     }
  //     );
  //     notifyListeners();
  //   }
  // }
  //
  //
  // Future<void> searchAndSetProducts({String? keyword,List? inventoryType}) async {
  //   var qString = "${AppConfig.baseUrl + AppConfig.productFetchApi}?page_size=$perPageItem&page=$pageCount&product_price_category_id=1&category_id=0&bran""ch_id=1&sort_order=desc";
  //   try {
  //     setBusy(true);
  //     var data = await _productService.searchProducts(keyword,qString,loggedInUser!.userToken!,inventoryType);
  //     setBusy(false);
  //     data.fold(
  //             (failure){
  //           setErrorMessage(failure.message);
  //         },(data){
  //       var productData = data['data'];
  //       processData(productData);
  //     }
  //     );
  //     notifyListeners();
  //   }
  //   catch (error) {
  //     throw (error);
  //   }
  // }


  Future<void> fetchProducts() async {
    try {
      setBusy(true);
      var jsonData = await rootBundle.loadString('assets/products.json');
      var extractedData = json.decode(jsonData);
      setBusy(false);
      final List<Product> loadedProducts = [];
      if(extractedData['data']['data'].length > 0) {
        for (int i = 0; i < extractedData['data']['data'].length; i++) {
          final Product product = Product.fromJson(extractedData['data']['data'][i],'');
          loadedProducts.add(product);
        }
        _products = loadedProducts;
      }else{
        _products = [];
      }
      notifyListeners();
    }on SocketDirection{
      throw 'No data';
    }

  }
}