
import 'package:flutter/material.dart';

class Product with ChangeNotifier {
  String id;
  String title;
  String productCategoryId;
  String description;
  double price;
  double avgUnitCost;
  String unit;
  String quantityLeft;
  String imageUrl;
  int isNonInventory;
  String salesAccountsGroupId;
  // double discount;
  String discountId;
  String discountType;
  double perUnitDiscount;
  double vatRate;

  Product({
    required this.id,
    required this.title,
    required this.productCategoryId,
    required this.description,
    required this.price,
    required this.avgUnitCost,
    required this.unit,
    required this.quantityLeft,
    required this.imageUrl,
    required this.isNonInventory,
    required this.salesAccountsGroupId,
    // required this.discount,
    required this.discountId,
    required this.discountType,
    required this.perUnitDiscount,
    required this.vatRate,

  });


  factory Product.fromJson(Map<String, dynamic> data,var cdnUrl){
    return Product(
        id: data['id'].toString(),
        title: data['name'] != null ? data['name'] : 'no title',
        productCategoryId: data['product_category_id'] != null ? data['product_category_id'].toString() : '',
        description: data['description'] != null ? data['description'] : 'no description found',
        // unit: data['unit_name'] ? data['unit_name'] : 'no unit',
        unit: 'no unit',
        // quantityLeft: data['quantity_left'] != null ? data['quantity_left'].toString():'0.0',
        quantityLeft: '0.0',
        price: data['unit_price'] != null ? double.parse(data['unit_price'].toString()) : 0.0,
        avgUnitCost: data['average_unit_cost'] != null ? double.parse(data['average_unit_cost'].toString()) : 0.0,
        vatRate: data['vat_rate'] != null ? double.parse(data['vat_rate'].toString()) : 0.0,
        isNonInventory: data['inventory_type'] != null ?data['inventory_type'] : 1,
        salesAccountsGroupId: data['sales_accounts_group_id'].toString(),
        perUnitDiscount: data['discount']!=null ? double.parse(data['discount'].toString()): 0.0,
        discountType: data['discount_type'] != null ? data['discount_type'] : '',
        discountId: data['discount_id'] != null ? data['discount_id'].toString() : '',
        imageUrl: data['thumb_image'] != null
            ? cdnUrl  + data['thumb_image']:""
      // 'https://www.jessicagavin.com/wp-content/uploads/2019/02/honey-1-600x900.jpg',
    );

  }


  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': title,
      'product_category_id': productCategoryId,
      'description': description,
      'unit_name':unit,
      'unit_price':price,
      'average_unit_cost':avgUnitCost,
      'vat_rate':vatRate,
      'is_non_inventory':isNonInventory,
      'sales_accounts_group_id':salesAccountsGroupId,
      'discount':perUnitDiscount,
      'discount_type':discountType,
      'discount_id':discountId,
      'thumb_image':imageUrl
    };
  }

}