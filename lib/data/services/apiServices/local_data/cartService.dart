

import 'package:delivery_app/data/dataHelper/local_db_helper.dart';
import 'package:delivery_app/data/services/apiServices/base_service.dart';
import 'package:delivery_app/domain/models/cartItem.dart';
import 'package:stacked/stacked.dart';
import 'package:injectable/injectable.dart';
import 'package:dartz/dartz.dart';
import 'package:delivery_app/data/dataHelper/error_handler.dart';
import 'package:delivery_app/data/dataHelper/failure.dart';




@lazySingleton
class CartService with ReactiveServiceMixin{

  CartService(){
    listenToReactiveValues([_cartItems,_totalAmount]);
  }

  final _cartItems = ReactiveValue<List<CartItem>>([]);
  List<CartItem> get cartItems => _cartItems.value;

  final _totalAmount = ReactiveValue<double>(0.0);
  double get totalAmount => _totalAmount.value;



  Future<void> fetchAndSetCartItems() async {

    final dataList = await DBHelper.getData('cartTable');
    _cartItems.value = dataList
        .map(
          (item) => CartItem(
        id: item['id'].toString(),
        productId: item['productId'],
        invoiceDetailsId: item['invoiceDetailsId'],
        title: item['title'],
        productCategoryId:item['productCategoryId'],
        quantity: item['quantity'].toDouble(),
        stockQuantity: double.parse(item['stockQuantity'].toString()),
        // unitName: item['unitName'],
        price: item['price'].toDouble(),
        avgUnitCost: item['avgUnitCost'] != null ? item['avgUnitCost'].toDouble():0.0,
        isNonInventory: item['isNonInventory'],
        salesAccountsGroupId:item['salesAccountsGroupId'],
        discount: item['discount'] != null? item['discount'].toDouble():0.0,
        discountType: item['discountType'],
        discountId: item['discountId'],
        perUnitDiscount: item['perUnitDiscount'] !=null ? item['perUnitDiscount'].toDouble():0.0,
        perUnitPercentDiscount: item['perUnitPercentDiscount'] !=null ? item['perUnitPercentDiscount'].toDouble():0.0,
        perUnitOverAllDiscount: item['perUnitOverAllDiscount'] !=null ? item['perUnitOverAllDiscount'].toDouble():0.0,
        itemTotalDiscount: item['totalDiscount'] !=null ? item['totalDiscount'].toDouble():0.0,
        overallDiscount: item['overAllDiscount'] !=null ? item['overAllDiscount'].toDouble():0.0,
        vatRate: item['vatRate'] != null ? item['vatRate'].toDouble():0.0,
        orderId: item['orderId'],
      ),
    ).toList();
    print('cart item count : ' + _cartItems.value.length.toString());
  }

  Future<void> addItem(
      String productId,
      String title,
      // String productCategoryId,
      String unitName,
      double stockQuantity,
      double price,
      double avgUnitCost,
      int isNonInventory,
      String salesAccountsGroupId,
      double discount,
      String discountId,
      String discountType,
      double perUnitDiscount,
      double vatRate
      ) async {
    bool item = await DBHelper.isProductExist(productId,'cartTable');

    if (!item) {
      await DBHelper.insert('cartTable', {
        'productId': productId,
        'title': title,
        'quantity': 1,
        'stockQuantity': stockQuantity,
        'unitName': unitName,
        'price': price,
        'avgUnitCost':avgUnitCost,
        'isNonInventory': isNonInventory,
        'salesAccountsGroupId': salesAccountsGroupId,
        'discount': discount,
        'discountType': discountType,
        'discountId': discountId,
        'perUnitDiscount': perUnitDiscount,
        'vatRate': vatRate,
        'orderId': '',
      });
    } else {
      await DBHelper.increaseItemQuantity('cartTable', productId);
    }
  }

  Future<void> removeSingleItem(String productId) async {
    CartItem? cartData = await DBHelper.getSingleData(productId);
    if (cartData?.quantity == 1) {
      await DBHelper.deleteCartItm(productId,'cartTable');
    } else {
      await DBHelper.decreaseItemQuantity(productId);
    }
  }

  Future<void> updateSingleCartItem(String productId, double quantity) async{
    // CartItem cartData = await DBHelper.getSingleData(productId);
    await DBHelper.updateItemQuantity('cartTable',productId, quantity);
  }

  Future<void> updateCartItem(String productId, double quantity,double totalDiscount, double overAllDistocunt) async{
    await DBHelper.updateCartItem(productId, quantity,totalDiscount,overAllDistocunt);
  }

  Future<void> removeCartItemRow(String productId) async {
    await DBHelper.deleteCartItm(productId,'cartTable');
  }

  Future<void> clearCart() async {
    await DBHelper.clearCart('cartTable');
  }

  // void getTotalAmount() async{
  //   var total = 0.0;
  //   await fetchAndSetCartItems();
  //   _cartItems.value.forEach((item) {
  //     total += item.price.toDouble() * item.quantity;
  //   });
  //    _totalAmount.value = total;
  // }

  Future<void> getTotalAmount() async{
    var total = 0.0;
    await fetchAndSetCartItems();
    _cartItems.value.forEach((item) {
      total += item.price.toDouble() * item.quantity;
    });
    _totalAmount.value = total;
  }


  CartItem getFindById(String id) {
    return _cartItems.value.firstWhere((item) => item.id == id);
  }

}












// @lazySingleton
// class CartService with ListenableServiceMixin{
//
//   CartService(){
//    listenToReactiveValues([_cartItems,_totalAmount]);
//   }
//
//   final _cartItems = ReactiveValue<List<CartItem>>([]);
//   List<CartItem> get cartItems => _cartItems.value;
//
//   final _totalAmount = ReactiveValue<double>(0.0);
//   double get totalAmount => _totalAmount.value;
//
//   void setTotalAmount(double value){
//     _totalAmount.value = value;
//     notifyListeners();
//   }
//
//   // static List<CartItem> _cartItems = [];
//   // List<CartItem> get cartItems => _cartItems;
//   //
//   // static double _totalAmount = 0.0;
//   // double get totalAmount => _totalAmount;
//
//
//
//   Future<dynamic> fetchAndSetCartItems() async {
//     Map<String,dynamic> responseData= {};
//     final dataList = await DBHelper.getData('cartTable');
//
//     try {
//       var response = dataList.map((item) => CartItem(
//           id: item['id'].toString(),
//           productId: item['productId'],
//           invoiceDetailsId: item['invoiceDetailsId'],
//           title: item['title'],
//           productCategoryId:item['productCategoryId'],
//           quantity: item['quantity'].toDouble(),
//           stockQuantity: double.parse(item['stockQuantity'].toString()),
//           unitName: item['unitName'],
//           price: item['price'].toDouble(),
//           avgUnitCost: item['avgUnitCost'] != null ? item['avgUnitCost'].toDouble():0.0,
//           isNonInventory: item['isNonInventory'],
//           salesAccountsGroupId:item['salesAccountsGroupId'],
//           discount: item['discount'] != null? item['discount'].toDouble():0.0,
//           discountType: item['discountType'],
//           discountId: item['discountId'],
//           perUnitDiscount: item['perUnitDiscount'] !=null ? item['perUnitDiscount'].toDouble():0.0,
//           perUnitPercentDiscount: item['perUnitPercentDiscount'] !=null ? item['perUnitPercentDiscount'].toDouble():0.0,
//           perUnitOverAllDiscount: item['perUnitOverAllDiscount'] !=null ? item['perUnitOverAllDiscount'].toDouble():0.0,
//           itemTotalDiscount: item['totalDiscount'] !=null ? item['totalDiscount'].toDouble():0.0,
//           overallDiscount: item['overAllDiscount'] !=null ? item['overAllDiscount'].toDouble():0.0,
//           vatRate: item['vatRate'] != null ? item['vatRate'].toDouble():0.0,
//           orderId: item['orderId'],
//         )).toList();
//       if(response.isNotEmpty) {
//         responseData.putIfAbsent('data', () => response);
//         return Right(responseData);
//       }else{
//         return Left(FailureItem( 404, ResponseMessage.DEFAULT)
//         );
//       }
//     } catch (error) {
//       return (Left(ErrorHandler.handle(error).failure));
//     }
//   }
//
//   Future<void> addItem(
//       String productId,
//       String title,
//       // String productCategoryId,
//       String unitName,
//       double stockQuantity,
//       double price,
//       double avgUnitCost,
//       int isNonInventory,
//       String salesAccountsGroupId,
//       double discount,
//       String discountId,
//       String discountType,
//       double perUnitDiscount,
//       double vatRate
//       ) async {
//     bool item = await DBHelper.isProductExist(productId,'cartTable');
//
//     if (!item) {
//       await DBHelper.insert('cartTable', {
//         'productId': productId,
//         'title': title,
//         'quantity': 1,
//         'stockQuantity': stockQuantity,
//         'unitName': unitName,
//         'price': price,
//         'avgUnitCost':avgUnitCost,
//         'isNonInventory': isNonInventory,
//         'salesAccountsGroupId': salesAccountsGroupId,
//         'discount': discount,
//         'discountType': discountType,
//         'discountId': discountId,
//         'perUnitDiscount': perUnitDiscount,
//         'vatRate': vatRate,
//         'orderId': '',
//       });
//     } else {
//       await DBHelper.increaseItemQuantity('cartTable', productId);
//     }
//   }
//
//   Future<void> removeSingleItem(String productId) async {
//     CartItem? cartData = await DBHelper.getSingleData(productId);
//     if (cartData?.quantity == 1) {
//       await DBHelper.deleteCartItm(productId,'cartTable');
//     } else {
//       await DBHelper.decreaseItemQuantity(productId);
//     }
//   }
//
//   Future<void> updateSingleCartItem(String productId, double quantity) async{
//     // CartItem cartData = await DBHelper.getSingleData(productId);
//     await DBHelper.updateItemQuantity('cartTable',productId, quantity);
//   }
//
//   Future<void> updateCartItem(String productId, double quantity,double totalDiscount, double overAllDistocunt) async{
//     await DBHelper.updateCartItem(productId, quantity,totalDiscount,overAllDistocunt);
//   }
//
//   Future<void> removeCartItemRow(String productId) async {
//     await DBHelper.deleteCartItm(productId,'cartTable');
//   }
//
//   Future<void> clearCart() async {
//     await DBHelper.clearCart('cartTable');
//   }
//
//   // // void getTotalAmount() async{
//   // //   var total = 0.0;
//   // //   await fetchAndSetCartItems();
//   // //   _cartItems.value.forEach((item) {
//   // //     total += item.price.toDouble() * item.quantity;
//   // //   });
//   // //    _totalAmount.value = total;
//   // // }
//   //
//   // void getTotalAmount() async{
//   //   var total = 0.0;
//   //   await fetchAndSetCartItems();
//   //   _cartItems.forEach((item) {
//   //     // total += item.price.toDouble() * item.quantity;
//   //   });
//   //   _totalAmount = total;
//   // }
//   //
//   //
//   // CartItem getFindById(String id) {
//   //   return _cartItems.firstWhere((item) => item.id == id);
//   // }
//
// }

