
import 'package:delivery_app/app/di/locator.dart';
import 'package:delivery_app/data/services/apiServices/base_service.dart';
import 'package:delivery_app/data/services/helperServices/dio_service.dart';
import 'package:delivery_app/data/dataHelper/error_handler.dart';
import 'package:delivery_app/data/dataHelper/failure.dart';
import 'package:dartz/dartz.dart';



class AuthService extends BaseService{

  final DioService _dioFactory = locator<DioService>();

  Future<dynamic> loginUser(var url,var authData) async {

    Map<String,dynamic> responseData= {};

    try {

      final response = await _dioFactory.postDioWithOutToken(url,authData);
      if(response.statusCode == ResponseCode.SUCCESS) {
        responseData.putIfAbsent('data', () => response.data);
        return Right(responseData);
      }else{
        return Left(FailureItem(
            // response.statusCode ?? ApiInternalStatus.FAILURE,
            response.statusCode ?? 404,
            response.statusMessage ?? ResponseMessage.DEFAULT)
        );
      }
    } catch (error) {
      return (Left(ErrorHandler.handle(error).failure));
    }
  }


  Future<dynamic> logoutUser(var url,var token,var authData) async {

    Map<String,dynamic> responseData= {};

    try {

      final response = await _dioFactory.postDioWithToken(url:url,token:token);
      if(response.statusCode == ResponseCode.SUCCESS) {
        responseData.putIfAbsent('data', () => response.data);
        return Right(responseData);
      }else{
        return Left(FailureItem(
          // response.statusCode ?? ApiInternalStatus.FAILURE,
            response.statusCode ?? 404,
            response.statusMessage ?? ResponseMessage.DEFAULT)
        );
      }
    } catch (error) {
      return (Left(ErrorHandler.handle(error).failure));
    }
  }

}